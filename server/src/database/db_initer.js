/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const { Logger } = require('../utils');

const dbs = {};
fs.readdirSync(__dirname, { withFileTypes: true })
  .filter((file) => file.isDirectory() && /[^\.]+\.database$/.test(file.name))
  .map((file) => file.name.replace('.database', ''))
  .forEach((databaseName) => {
    dbs[databaseName] = {
      getModel: (modelName) => dbs[databaseName][modelName],
    };
  });

class DbsIniter {

  static async init() {
    try {
      await Promise.each(Object.keys(dbs), async (dbName) => {
        const databaseConfig = require(`${__dirname}/`
          + `${dbName}.database/config.js`);
        databaseConfig.config.dialectOptions = {
          dateStrings: true,
          typeCast: true,
        };
        databaseConfig.config.timezone = 'Europe/Paris';
        const currentDatabaseConnector = new Sequelize(
          databaseConfig.name,
          databaseConfig.user,
          databaseConfig.password,
          databaseConfig.config,
        );
        await currentDatabaseConnector.authenticate();
        fs.readdirSync(`${__dirname}/${dbName}.database`)
          .filter((file) => /[^\.]+\.model\.js$/.test(file))
          .forEach((modelName) => {
            const model = require(
              path.join(`${__dirname}/${dbName}.database/`, modelName),
            )(currentDatabaseConnector, Sequelize.DataTypes);
            dbs[dbName][model.name] = model;
          });
        Object.keys(dbs[dbName]).forEach((modelName) => {
          if (dbs[dbName][modelName].associate) {
            dbs[dbName][modelName].associate(dbs[dbName]);
          }
        });
        currentDatabaseConnector.sync();
        dbs[dbName].connector = currentDatabaseConnector;
      });
    } catch (err) {
      Logger.error('DATABASE ERROR:', err);
    }
  }

}

module.exports = {
  dbs,
  DbsIniter,
};
