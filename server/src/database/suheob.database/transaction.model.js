module.exports = (sequelize, { UUID, STRING, FLOAT, DATE, ARRAY, TEXT }) => {
  const transaction = sequelize.define('transaction', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: UUID,
    },
    sessionId: STRING,
    pack: ARRAY(TEXT),
    amount: FLOAT,
    status: STRING,
    sessionSuccessTime: DATE,
    sessionErrorTime: DATE,

  }, {
    underscored: true,
    freezeTableName: true,
  });
  transaction.associate = (models) => {
    models.transaction.belongsTo(models.user, {
      onDelete: 'CASCADE',
      foreignKey: 'userId',
      targetKey: 'id',
    });
  };
  return transaction;
};