module.exports = (sequelize, { UUID, STRING, JSON }) => {

  const image = sequelize.define('image', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: UUID,
    },
    name: STRING,
    url: STRING,
  }, {
    underscored: true,
    freezeTableName: true,
  });
  image.associate = (models) => {
    models.image.belongsTo(models.course, {
      onDelete: 'CASCADE',
      foreignKey: 'courseId',
      targetKey: 'id',
    });
  };

  return image;
};