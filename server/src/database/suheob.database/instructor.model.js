module.exports = (sequelize, { UUID }) => {
  const instructor = sequelize.define('instructor', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: UUID,
    },
  }, {
    underscored: true,
    freezeTableName: true,
  });
  instructor.associate = (models) => {
    models.instructor.hasMany(models.course);
  };

  return instructor;
};