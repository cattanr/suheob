module.exports = (sequelize, { UUID, STRING, JSON, FLOAT }) => {

  const chapter = sequelize.define('chapter', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: UUID,
    },
    title: STRING,
    block_config: JSON,
  }, {
    underscored: true,
    freezeTableName: true,
  });
  chapter.associate = (models) => {
    models.chapter.belongsTo(models.course, {
      onDelete: 'CASCADE',
      foreignKey: 'courseId',
      targetKey: 'id',
    });
  };

  return chapter;
};