module.exports = (sequelize, { UUID, STRING, JSON }) => {

  const review = sequelize.define('review', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: UUID,
    },
    status: STRING,
    validationSchema: JSON,
    rejectionReason: STRING,
  }, {
    underscored: true,
    freezeTableName: true,
  });
  review.associate = (models) => {
    models.review.belongsTo(models.course, {
      onDelete: 'CASCADE',
      foreignKey: 'courseId',
      targetKey: 'id',
    });
  };

  return review;
};