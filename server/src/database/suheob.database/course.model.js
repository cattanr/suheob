module.exports = (sequelize, { UUID, STRING, JSON, FLOAT }) => {

  const course = sequelize.define('course', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: UUID,
    },
    status: STRING,
    config: JSON,
    title: STRING,
    description: STRING,
    mainImage: STRING,
    price: FLOAT,
  }, {
    underscored: true,
    freezeTableName: true,
  });
  course.associate = (models) => {
    models.course.hasMany(models.chapter);
    models.course.hasMany(models.review);
    models.course.hasMany(models.image);
    models.course.belongsToMany(models.user, { through: 'user_course' });
    models.course.belongsTo(models.instructor, {
      onDelete: 'CASCADE',
      foreignKey: 'instructorId',
      targetKey: 'id',
    });
  };

  return course;
};