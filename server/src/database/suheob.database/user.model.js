module.exports = (sequelize, { UUID }) => {
  const user = sequelize.define('user', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: UUID,
    },
  }, {
    underscored: true,
    freezeTableName: true,
  });
  user.associate = (models) => {
    models.user.hasMany(models.transaction);
    models.user.belongsToMany(models.course, { through: 'user_course' });
  };

  return user;
};