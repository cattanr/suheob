const {
  SUHEOB_NAME,
  SUHEOB_USER,
  SUHEOB_PASSWORD,
  SUHEOB_DIALECT,
  SUHEOB_HOST,
  SUHEOB_PORT,
} = process.env;

module.exports = {
  name: SUHEOB_NAME,
  user: SUHEOB_USER,
  password: SUHEOB_PASSWORD,
  config: {
    dialect: SUHEOB_DIALECT,
    host: SUHEOB_HOST,
    port: SUHEOB_PORT,
    logging: false,
  },
};
