const {
  KEYCLOAK_NAME,
  KEYCLOAK_USER,
  KEYCLOAK_PASSWORD,
  KEYCLOAK_HOST,
  KEYCLOAK_PORT,
  KEYCLOAK_DIALECT,
} = process.env;

module.exports = {
  name: KEYCLOAK_NAME,
  user: KEYCLOAK_USER,
  password: KEYCLOAK_PASSWORD,
  config: {
    dialect: KEYCLOAK_DIALECT,
    host: KEYCLOAK_HOST,
    port: KEYCLOAK_PORT,
    logging: false,
  },
};
