module.exports = (sequelize, { DATE, UUID, STRING, BOOLEAN }) => {

  const user_entity = sequelize.define('user_entity', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: UUID,
    },
    email: STRING,
    email_verified: BOOLEAN,
    enabled: BOOLEAN,
    first_name: STRING,
    last_name: STRING,
    realm_id: STRING,
    username: STRING,
    created_timestamp: DATE,
  }, {
    timestamps: false,
    freezeTableName: true,
  });

  return user_entity;
};