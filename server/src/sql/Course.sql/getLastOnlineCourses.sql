SELECT * FROM "course" AS course
LEFT JOIN "user_course" AS user_course ON user_course.course_id = course.id
WHERE course.id NOT IN (
	SELECT course_id FROM "user_course" AS user_courseb 
	WHERE user_courseb.user_id = :userId
)
AND course.status = 'LIVE'
LIMIT :maxLimit