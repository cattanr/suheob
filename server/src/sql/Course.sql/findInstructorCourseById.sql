SELECT course.id, course.status, course.config FROM "course" as course
LEFT JOIN "user" as instructor ON course.instructor_id = instructor.id
WHERE course.id = :id
AND instructor.keycloak_id = :token
LIMIT 1