const HealthCheckService = require('../service/HealthCheck.service');

module.exports = class healthCheckController {

  static async check() {
    return HealthCheckService.check();
  }

};