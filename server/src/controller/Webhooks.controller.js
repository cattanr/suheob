const WebhooksService = require('../service/Webhooks.service');
const { InternalServerError } = require('../utils');

module.exports = class WebhooksController {

  static async handle(req) {
    try {
      return WebhooksService.handle(req);
    }
    catch (error) {
      throw new InternalServerError(error);
    }
  }

};