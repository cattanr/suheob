const CheckoutService = require('../service/Checkout.service');
const { InternalServerError } = require('../utils');

module.exports = class CheckoutController {

  static async create(req) {
    try {
      if (req.kauth.grant.access_token.content.sub) {
        return CheckoutService.create(
          req.body,
          req.kauth.grant.access_token.content.sub,
        );
      }
    } catch (error) {
      throw new InternalServerError(error);
    }
  }

};