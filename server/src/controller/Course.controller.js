const CourseService = require('../service/Course.service');
const { InternalServerError } = require('../utils');
const { renderOk } = require('../utils');

module.exports = class CourseController {

  static async getCourse(req) {
    try {
      if (req.kauth.grant.access_token.content.sub) {
        return CourseService.getCourse(
          req.kauth.grant.access_token.content.sub,
          req.params.id,
        );
      }
    } catch (error) {
      throw new InternalServerError(error);
    }
  }

  static async getPublicCourse(req) {
    try {
      return CourseService.getPublicCourse(req.params.id);
    } catch (error) {
      throw new InternalServerError(error);
    }
  }

  static async create(req) {
    try {
      if (req.kauth.grant.access_token.content.sub) {
        return CourseService.create(
          req.kauth.grant.access_token.content.sub,
        );
      }
    } catch (error) {
      throw new InternalServerError(error);
    }
  }

  static async getPublicLastOnlineCourses(req) {
    try {
      return CourseService.getPublicLastOnlineCourses();
    } catch (error) {
      throw new InternalServerError(error);
    }
  }

  static async getLastOnlineCourses(req) {
    try {
      if (req.kauth.grant.access_token.content.sub) {
        return CourseService.getLastOnlineCourses(req.kauth.grant.access_token.content.sub);
      }
    } catch (error) {
      throw new InternalServerError(error);
    }
  }

  static async getMyCourse(req) {
    try {
      if (req.kauth.grant.access_token.content.sub) {
        return CourseService.getMyCourse(
          req.kauth.grant.access_token.content.sub,
          req.params.id,
        );
      }
    } catch (error) {
      throw new InternalServerError(error);
    }
  }

  static async getMyCourses(req) {
    try {
      if (req.kauth.grant.access_token.content.sub) {
        return CourseService.getMyCourses(
          req.kauth.grant.access_token.content.sub,
        );
      }
    } catch (error) {
      throw new InternalServerError(error);
    }
  }

  static async uploadMainImage(req) {
    try {
      if (req.kauth.grant.access_token.content.sub) {
        if (!req.files || Object.keys(req.files).length === 0) {
          return 'No files were uploaded.';
        }
        const { file } = req.files;
        const { id } = req.params;
        return CourseService.uploadMainImage(
          req.kauth.grant.access_token.content.sub,
          id,
          file,
        );
      }
    } catch (error) {
      throw new InternalServerError(error);
    }
  }

  static async uploadImage(req, res) {
    try {
      if (req.kauth.grant.access_token.content.sub) {
        if (!req.files || Object.keys(req.files).length === 0) {
          return 'No files were uploaded.';
        }
        const { file } = req.files;
        const { id } = req.params;
        const url = await CourseService.uploadImage(
          req.kauth.grant.access_token.content.sub,
          id,
          file,
        );
        renderOk(res, url);
      }
    } catch (error) {
      throw new InternalServerError(error);
    }
  }

  static async getInstructorCourse(req) {
    try {
      if (req.kauth.grant.access_token.content.sub) {
        return CourseService.getInstructorCourse(
          req.kauth.grant.access_token.content.sub,
          req.params.id,
        );
      }
    } catch (error) {
      throw new InternalServerError(error);
    }
  }

  static async update(req) {
    try {
      if (req.kauth.grant.access_token.content.sub) {
        return CourseService.update(
          req.params.id,
          req.body,
          req.kauth.grant.access_token.content.sub,
        );
      }
    } catch (error) {
      throw new InternalServerError(error);
    }
  }

  static async createChapters(req) {
    try {
      if (req.kauth.grant.access_token.content.sub) {
        return CourseService.createChapters(
          req.kauth.grant.access_token.content.sub,
          req.params.id,
          req.body,
        );
      }
    } catch (error) {
      throw new InternalServerError(error);
    }
  }

  static async getChapters(req) {
    try {
      if (req.kauth.grant.access_token.content.sub) {
        return CourseService.getChapters(
          req.kauth.grant.access_token.content.sub,
          req.params.id,
        );
      }
    } catch (error) {
      throw new InternalServerError(error);
    }
  }

  static async removeChapter(req) {
    try {
      if (req.kauth.grant.access_token.content.sub) {
        const { id, chapterId } = req.params;
        return CourseService.removeChapter(
          req.kauth.grant.access_token.content.sub,
          id,
          chapterId,
        );
      }
    } catch (error) {
      throw new InternalServerError(error);
    }
  }

  static async review(req) {
    try {
      if (req.kauth.grant.access_token.content.sub) {
        return CourseService.review(
          req.kauth.grant.access_token.content.sub,
          req.params.id,
        );
      }
    } catch (error) {
      throw new InternalServerError(error);
    }
  }

  static async getInstructorCourses(req) {
    try {
      if (req.kauth.grant.access_token.content.sub) {
        return CourseService.getInstructorCourses(
          req.kauth.grant.access_token.content.sub,
        );
      }
    } catch (error) {
      throw new InternalServerError(error);
    }
  }

};