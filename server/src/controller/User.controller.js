const path = require('path');
const fs = require('fs');
const UserService = require('../service/User.service');
const { InternalServerError } = require('../utils');

module.exports = class UserController {

  static async getImageByName(req, res) {
    try {
      const { userId, courseId, name } = req.params;
      const imageName = await UserService.getImageByName(
        userId,
        courseId,
        name,
      );
      if (imageName) {
        const img = fs.readFileSync(path.resolve(`/app/src/course/images/${imageName}`));
        res.set('Content-Type', `image/${imageName.match(/\.(.+)$/)[1]}`);
        res.send(img);
      }
    } catch (error) {
      throw new InternalServerError(error);
    }
  }

};