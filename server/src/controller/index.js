const HealthCheck = require('./HealthCheck.controller');
const Checkout = require('./Checkout.controller');
const Webhooks = require('./Webhooks.controller');
const Course = require('./Course.controller');
const User = require('./User.controller');

module.exports = {
  HealthCheck,
  Checkout,
  Webhooks,
  Course,
  User,
};