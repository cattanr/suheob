const Joi = require('joi');

module.exports = Joi.object({
  chapters: Joi.array()
    .items({
      id: Joi.string().guid({ version: 'uuidv4' }).required(),
      title: Joi.string(),
      blocks: Joi.array()
        .items({
          id: Joi.string().guid({ version: 'uuidv4' }),
          content: Joi.string().allow(''),
          type: Joi.string(),
        }),
    }),
});