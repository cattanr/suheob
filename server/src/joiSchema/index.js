const courseUpdateJoiSchema = require('./courseUpdate.joiSchema');
const chapterCreationJoiSchema = require('./chapterCreation.joiSchema');
const courseReviewJoiSchema = require('./courseReview.joiSchema');
const checkoutCreationJoiSchema = require('./checkoutCreation.schema');

module.exports = {
  courseUpdateJoiSchema,
  chapterCreationJoiSchema,
  courseReviewJoiSchema,
  checkoutCreationJoiSchema,
};
