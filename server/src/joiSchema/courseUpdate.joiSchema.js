const Joi = require('joi');

module.exports = Joi.object({
  title: Joi.string(),
  config: Joi.string(),
  description: Joi.string(),
  price: Joi.number(),
  mainImage: Joi.string(),
});
