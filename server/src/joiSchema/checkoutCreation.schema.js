const Joi = require('joi');

module.exports = Joi.object({
  cart: Joi.array().items({
    id: Joi.string().guid({ version: 'uuidv4' }).required(),
    title: Joi.string(),
    description: Joi.string(),
    main_image: Joi.string(),
    price: Joi.number().allow(0),
    instructor_id: Joi.string(),
    users: Joi.array(),
    creator: Joi.string(),
  }),
  email: Joi.string().email(),
});