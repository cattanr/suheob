const Joi = require('joi');

module.exports = Joi.object({
  title: Joi.string().required(),
  description: Joi.string().required(),
  mainImage: Joi.string().required(),
  price: Joi.number().positive().precision(2).allow(0),
});