const session = require('express-session');

const { SESSION_SECRET } = process.env;

const init = (store) => {
  return session({
    secret: SESSION_SECRET,
    resave: false,
    saveUninitialized: true,
    store,
  });
};

module.exports = {
  init,
};