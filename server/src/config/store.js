const session = require('express-session');
const redis = require('redis');
const RedisStore = require('connect-redis')(session);
const { Logger } = require('../utils');

let _redisClient;

const { REDIS_URL } = process.env;

const init = () => {
  if (process.env.CACHE_IN_MEMORY === 'false' && REDIS_URL) {
    Logger.info('init Redis Cache');
    _redisClient = redis.createClient(REDIS_URL);
    return new RedisStore({ client: _redisClient });
  }
  Logger.info('init in Memory Cache');
  return new session.MemoryStore();

};

const getRedisClient = () => {
  if (!_redisClient) {
    Logger.error('Redis client has not been initialized. Please called init first.');
  }
  return _redisClient;
};

module.exports = {
  init,
  getRedisClient,
};