const Keycloak = require('keycloak-connect');
const { Logger } = require('../utils');

let _keycloak;

const { KEYCLOAK_BASE_URL, KEYCLOAK_CLIENT_ID, KEYCLOAK_CLIENT_SECRET,
  KEYCLOAK_REALM } = process.env;

const getKeycloakConfig = () => {
  if (
    !KEYCLOAK_BASE_URL
    || !KEYCLOAK_CLIENT_ID
    || !KEYCLOAK_CLIENT_SECRET
    || !KEYCLOAK_REALM
  ) {
    Logger.error(
      'Could not start keycloak configuration, some mandatory environment vars are missing',
    );
    process.exit();
  }
  return {
    clientId: KEYCLOAK_CLIENT_ID,
    bearerOnly: true,
    serverUrl: `${KEYCLOAK_BASE_URL}/auth`,
    realm: KEYCLOAK_REALM,
    credentials: {
      secret: KEYCLOAK_CLIENT_SECRET,
    },
  };
};

const init = (store) => {
  const keycloakConfig = getKeycloakConfig();
  if (_keycloak) {
    Logger.info('Trying to init Keycloak again!');
    return _keycloak;
  }
  Logger.info('Initializing Keycloak adapter...');
  _keycloak = new Keycloak({ store }, keycloakConfig);
  return _keycloak;

};

const getKeycloak = () => {
  if (!_keycloak) {
    Logger.error('Keycloak adapter has not been initialized. Please init first.');
  }
  return _keycloak;
};

module.exports = {
  init,
  getKeycloak,
};