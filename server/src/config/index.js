const keycloackConfig = require('./keycloak');
const storeConfig = require('./store');
const sessionConfig = require('./session');

module.exports = {
  keycloackConfig,
  storeConfig,
  sessionConfig,
};