const { suheob: suheobDb } = require('../database');

let InstructorModel;
module.exports = class InstructorService {

  static init() {
    if (InstructorModel === undefined) InstructorModel = suheobDb.getModel('instructor');
  }

  static async findOrCreate(keycloakId) {
    InstructorService.init();
    let instructor = await InstructorModel.findOne({
      where: {
        id: keycloakId,
      },
    });
    if (instructor === null) {
      const userObj = { id: keycloakId };
      instructor = await InstructorModel.create(userObj);
    }
    return instructor;
  }

};