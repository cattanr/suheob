const { keycloak: keycloakDb } = require('../database');

let CreatorModel;
module.exports = class CreatorService {

  static init() {
    if (CreatorModel === undefined) CreatorModel = keycloakDb.getModel('user_entity');
  }

  static async find(id) {
    CreatorService.init();
    const creator = await CreatorModel.findOne({ where: { id } });
    return creator;
  }

  static async agregateCreatorNameToCourse(course) {
    const { instructor_id } = course;
    const creator = await CreatorService.find(instructor_id);
    if (creator) {
      const { first_name, last_name } = creator;
      course.creator = `${first_name} ${last_name}`;
    }
    return course;
  }

  static async agregateCreatorNameToCourses(courses) {
    return Promise.map(courses, async (course) => {
      const { instructor_id } = course;
      const creator = await CreatorService.find(instructor_id);
      if (creator) {
        const { first_name, last_name } = creator;
        course.creator = `${first_name} ${last_name}`;
      }
      return course;
    });
  }

};
