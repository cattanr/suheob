const { suheob: suheobDb } = require('../database');
const { ForbiddenError } = require('../utils');

let UserModel;
module.exports = class UserService {

  static init() {
    if (UserModel === undefined) UserModel = suheobDb.getModel('user');
  }

  static async findOrCreate(keycloakId) {
    UserService.init();
    let user = await UserModel.findOne({
      where: {
        id: keycloakId,
      },
    });
    if (user === null) {
      const userObj = { id: keycloakId };
      user = await UserModel.create(userObj);
    }
    return user;
  }

  static async getImageByName(keycloakId, courseId, imageName) {
    UserService.init();
    // get le user not null
    const image = await suheobDb.getModel('image').findOne({
      where: { name: imageName, courseId },
    });
    if (image === null) {
      throw new ForbiddenError('You don\'t have access to this file');
    }
    return imageName;
  }

};