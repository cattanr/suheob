const { v4: uuidv4 } = require('uuid');
const path = require('path');
const { Op } = require('sequelize');
const { suheob: suheobDb } = require('../database');
const UserService = require('./User.service');
const ImageService = require('./Image.service');
const ChapterService = require('./Chapter.service');
const ReviewService = require('./Review.service');
const InstructorService = require('./Instructor.service');
const CreatorService = require('./Creator.service');
const { InternalServerError, NotFoundError, ForbiddenError } = require('../utils');
const { CourseSql } = require('../sql');
const {
  chapterCreationJoiSchema,
  courseReviewJoiSchema,
  courseUpdateJoiSchema,
} = require('../joiSchema');

let CourseModel;
module.exports = class CourseService {

  static init() {
    if (CourseModel === undefined) CourseModel = suheobDb.getModel('course');
  }

  static async getCourse(userId, id) {
    CourseService.init();
    const course = await CourseModel.findOne({
      attributes: ['id', 'title', 'description', 'main_image', 'price', 'instructor_id'],
      where: { id, status: 'LIVE' },
      include: {
        model: suheobDb.getModel('user'),
        through: {
          attributes: ['user_id'],
          where: { userId },
          required: false,
        },
      },
    });
    if (course) return CreatorService.agregateCreatorNameToCourse(course.get({ plain: true }));
    throw new NotFoundError({ message: 'Couldn\'t find a course with the provided id' });

  }

  static async getPublicCourse(id) {
    CourseService.init();
    const course = await CourseModel.findOne({
      attributes: ['id', 'title', 'description', 'main_image', 'price', 'instructor_id'],
      where: { id, status: 'LIVE' },
    });
    if (course) return CreatorService.agregateCreatorNameToCourse(course.get({ plain: true }));
    throw new NotFoundError({ message: 'Couldn\'t find a course with the provided id' });
  }

  static async create(keycloakId) {
    CourseService.init();
    const instructor = await InstructorService.findOrCreate(keycloakId);
    let course;
    if (instructor) {
      const courseObj = {
        id: uuidv4(),
        status: 'DRAFT',
        instructorId: instructor.id,
      };
      course = await CourseModel.create(courseObj);
    }
    return course;
  }

  static async getInstructorCourse(keycloakId, id) {
    const instructor = await InstructorService.findOrCreate(keycloakId);
    if (instructor) {
      CourseService.init();
      const course = await CourseModel.findOne({
        where: { id },
        include: [{
          model: suheobDb.getModel('instructor'),
          where: { id: keycloakId },
        }],
      });
      return course;
    }
    throw new ForbiddenError({ message: 'You are not allowed to access this ressource' });
  }

  static async getPublicLastOnlineCourses() {
    CourseService.init();
    const courses = await CourseModel.findAll({
      raw: true,
      attributes: ['id', 'title', 'description', 'main_image', 'price', 'instructor_id'],
      where: {
        status: 'LIVE',
      },
      order: [['updatedAt', 'DESC']],
      limit: 10,
    });
    if (courses) return CreatorService.agregateCreatorNameToCourses(courses);
    throw new InternalServerError({ message: 'Something went wrong fetching courses' });
  }

  static async getLastOnlineCourses(keycloakId) {
    CourseService.init();
    const courses = await suheobDb.connector.query(CourseSql.getLastOnlineCourses, {
      replacements: { userId: keycloakId, maxLimit: 10 },
    });
    if (courses[0]) return CreatorService.agregateCreatorNameToCourses(courses[0]);
    throw new InternalServerError({ message: 'Something went wrong fetching courses' });
  }

  static async getMyCourse(keycloakId, id) {
    const user = await UserService.findOrCreate(keycloakId);
    if (user) {
      CourseService.init();
      const course = await CourseModel.findOne({
        attributes: ['id', 'title', 'config', 'description', 'main_image', 'instructor_id'],
        where: { id, status: 'LIVE' },
        include: [{
          model: suheobDb.getModel('chapter'),
          attributes: ['id', 'title', 'block_config'],
        }],
      });
      if (course) return CreatorService.agregateCreatorNameToCourse(course.get({ plain: true }));
      throw new InternalServerError({ message: 'Something went wrong fetching courses' });
    }
  }

  static async getMyCourses(keycloakId) {
    const user = await UserService.findOrCreate(keycloakId);
    if (user) {
      CourseService.init();
      const courses = await CourseModel.findAll({
        raw: true,
        attributes: ['id', 'title', 'description', 'main_image', 'price', 'instructor_id'],
        where: { status: 'LIVE' },
        include: {
          model: suheobDb.getModel('user'),
          through: {
            attributes: ['userId'],
            where: { userId: keycloakId },
          },
          required: true,
        },
      });
      if (courses) return CreatorService.agregateCreatorNameToCourses(courses);
      throw new InternalServerError({ message: 'Something went wrong fetching courses' });
    }
  }

  static async update(id, data, keycloakId) {
    const instructor = await InstructorService.findOrCreate(keycloakId);
    if (instructor) {
      CourseService.init();
      const course = await CourseModel.findOne({ where: { id } });
      const { error } = courseUpdateJoiSchema.validate(data);
      if (error !== undefined) return error.details[0].message;
      const { title, config, description, mainImage } = data;
      let { price } = data;
      if (price > 0 && price % 1 !== 0) price = Math.floor(price * 100) / 100;
      const dataObj = { title, config, status: 'DRAFT', description, price, mainImage };
      Object.entries(dataObj).forEach(([key, value]) => {
        if (value === undefined || value === '') delete dataObj[key];
      });
      if (dataObj) {
        await course.update({ ...dataObj });
      }
      return course;
    }
  }

  static async uploadMainImage(keycloakId, id, file) {
    CourseService.init();
    let { name } = file;
    const fileExtension = name.slice(name.lastIndexOf('.'));
    name = `${id}${fileExtension}`;
    const uploadPath = path.resolve(`/app/src/public/images/${name}`);
    file.mv(uploadPath, (error) => {
      if (error) throw new InternalServerError({ message: error });
      const nameUrl = `${process.env.URL_SERVER}/images/${name}`;
      CourseService.update(id, { mainImage: nameUrl }, keycloakId);
      return 'file uploaded';
    });
  }

  static async uploadImage(keycloakId, courseId, file) {
    CourseService.init();
    const user = await suheobDb.getModel('user').findOne({ where: { id: keycloakId } });
    const course = await CourseModel.findOne({ where: { id: courseId } });
    if (user && course) {
      const id = uuidv4();
      let { name } = file;
      const fileExtension = name.slice(name.lastIndexOf('.'));
      name = `${id}${fileExtension}`;
      const uploadPath = path.resolve(`/app/src/course/images/${name}`);
      const url = `${process.env.URL_SERVER}/user/${user.id}/course/${courseId}/image/${name}`;
      file.mv(uploadPath, (error) => {
        if (error) throw new InternalServerError({ message: error });
      });
      const image = await ImageService.findOrCreate(user.id, name, url);
      image.update({ courseId: course.id });
      if (image === null) throw new InternalServerError({ message: 'something went wrong' });
      return url;
    }
    throw new NotFoundError({ message: 'Couldn\'t find a course with the provided id' });
  }

  static async createChapters(keycloakId, courseId, payload) {
    const { error } = chapterCreationJoiSchema.validate(payload);
    if (error !== undefined) return error.details[0].message;
    CourseService.init();
    const instructor = await InstructorService.findOrCreate(keycloakId);
    const course = await CourseModel.findOne({ where: { id: courseId } });
    if (instructor && course) {
      let config = { chapters: [] };
      payload.chapters.forEach(chapter => {
        ChapterService.findAndUpdateOrCreate(course.id, chapter);
        config.chapters.push(chapter.id);
      });
      config = JSON.stringify(config);
      CourseService.update(course.id, { config }, instructor.id);
    }
  }

  static async getChapters(userId, courseId) {
    const instructor = await suheobDb.getModel('instructor').findOne({ where: { id: userId } });
    CourseService.init();
    const userCourse = await CourseModel.findOne({
      attributes: ['id'],
      include: {
        model: suheobDb.getModel('user'),
        through: {
          attributes: ['userId', 'courseId'],
          where: { userId, courseId },
        },
        required: true,
      },
    });
    if (instructor || userCourse) {
      CourseService.init();
      const course = await CourseModel.findOne({ where: { id: courseId, instructor_id: userId } });
      if (course) {
        const chapters = await suheobDb.getModel('chapter').findAll({ where: { courseId } });
        const chaptersObj = { chapters: [] };
        if (chapters && chapters.length !== 0) {
          const config = JSON.parse(course.config);
          config.chapters.forEach((chapterId) => {
            const found = chapters.find(element => element.id === chapterId);
            if (found) {
              const { id, title, block_config } = found;
              const { blocks } = JSON.parse(block_config);
              chaptersObj.chapters.push({ id, title, blocks });
            }
          });
        }
        return chaptersObj;
      }
    }
    throw new NotFoundError({ message: 'You are not allowed to access this course' });
  }

  static async removeChapter(userId, courseId, chapterId) {
    const instructor = await suheobDb.getModel('instructor').findOne({ where: { id: userId } });
    if (instructor) {
      CourseService.init();
      let course = await CourseModel.findOne({ where: { id: courseId, instructor_id: userId } });
      if (course) {
        const isRemoved = await suheobDb.getModel('chapter').destroy({ where: { id: chapterId } });
        if (isRemoved === 1) {
          const config = JSON.parse(course.config);
          config.chapters.forEach((configChapterId, index) => {
            if (config.chapters.length !== 1 && configChapterId === chapterId) {
              config.chapters.splice(index, 1);
            }
          });
          course = await course.update({ config: JSON.stringify(config) });
          return course;
        }
      }
    }
    throw new ForbiddenError('You are not allowed to access this ressource');
  }

  static async review(userId, courseId) {
    const instructor = await suheobDb.getModel('instructor').findOne({ where: { id: userId } });
    if (instructor) {
      CourseService.init();
      const course = await CourseModel.findOne({ where: { id: courseId, instructor_id: userId } });
      if (course) {
        const { status, title, description, mainImage, price } = course;
        const courseValidation = {
          title, description, mainImage, price };
        const { error } = courseReviewJoiSchema.validate(courseValidation);
        if (error !== undefined) return error.details[0].message;
        let message;
        let review;
        switch (status) {
          case 'DRAFT':
            course.update({ status: 'IN_REVIEW' });
            review = await ReviewService.create(courseId);
            if (review === null) {
              throw new InternalServerError(
                { message: 'Something went wrong during the review creation' },
              );
            }
            message = 'Your course will be reviewed by our team as soon as possible.';
            break;
          case 'IN_REVIEW':
            message = 'Your review is pending validation, please wait our approbation';
            break;
          case 'LIVE':
            message = 'Your course have been approved and is now visible in our platform.';
            break;
          case 'REMOVED':
            message = 'The course you\'re looking for has been removed';
            break;
          default:
            course.update({ status: 'DRAFT' });
            break;
        }
        return { status, message };
      }
    }
  }

  static async getInstructorCourses(userId) {
    const instructor = await InstructorService.findOrCreate(userId);
    if (instructor) {
      CourseService.init();
      const courses = CourseModel.findAll({
        where: {
          instructor_id: userId,
          [Op.or]: [
            { status: 'DRAFT' },
            { status: 'LIVE' },
            { status: 'IN_REVIEW' },
          ],
        },
        order: [['updatedAt', 'ASC']],
      });
      return courses;
    }
    throw new ForbiddenError('You are not allowed to access this ressource');
  }

};