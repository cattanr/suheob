const { suheob: suheobDb } = require('../database');

let ChapterModel;
module.exports = class ChapterService {

  static init() {
    if (ChapterModel === undefined) ChapterModel = suheobDb.getModel('chapter');
  }

  static async findAndUpdateOrCreate(courseId, chapterToHandle) {
    ChapterService.init();
    let chapter = await ChapterModel.findOne({ where: { id: chapterToHandle.id } });
    const { id, title, blocks } = chapterToHandle;
    const blockConfig = { blocks };
    const chapterObj = { id, title, block_config: JSON.stringify(blockConfig), courseId };
    if (chapter === null) {
      chapter = await ChapterModel.create(chapterObj);
    }
    chapter = await chapter.update({ ...chapterObj });
    return chapter;

  }

};