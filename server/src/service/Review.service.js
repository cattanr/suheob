const { v4: uuidv4 } = require('uuid');
const { suheob: suheobDb } = require('../database');

let ReviewModel;
module.exports = class ReviewService {

  static init() {
    if (ReviewModel === undefined) ReviewModel = suheobDb.getModel('review');
  }

  static async create(courseId) {
    ReviewService.init();
    let review = await ReviewModel.findOne({
      where: {
        courseId,
        status: 'PENDING',
      },
    });
    if (review === null) {
      const reviewObj = { id: uuidv4(), status: 'PENDING', courseId };
      review = await ReviewModel.create(reviewObj);
    }
    return review;
  }

};