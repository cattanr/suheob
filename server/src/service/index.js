const HealCheckService = require('./HealthCheck.service');
const CheckoutService = require('./Checkout.service');
const WebhooksService = require('./Webhooks.service');
const TransactionService = require('./Transaction.service');
const UserService = require('./User.service');
const CourseService = require('./Course.service');
const ImageService = require('./Image.service');
const ChapterService = require('./Chapter.service');
const ReviewService = require('./Review.service');
const InstructorService = require('./Instructor.service');
const CreatorService = require('./Creator.service');

module.exports = {
  HealCheckService,
  CheckoutService,
  WebhooksService,
  TransactionService,
  UserService,
  CourseService,
  ImageService,
  ChapterService,
  ReviewService,
  InstructorService,
  CreatorService,
};