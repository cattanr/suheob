const { v4: uuidv4 } = require('uuid');
const moment = require('moment');
const { suheob: suheobDb } = require('../database');

let TransactionModel;
module.exports = class TransactionService {

  static init() {
    if (TransactionModel === undefined) TransactionModel = suheobDb.getModel('transaction');
  }

  static async create(userId, pack, amount, sessionId) {
    TransactionService.init();
    const transactionObj = { id: uuidv4(), userId, status: 'CREATED', pack, amount, sessionId };
    const transaction = await TransactionModel.create(transactionObj);
    return transaction;
  }

  static async updateStatusById(id, status) {
    TransactionService.init();
    let transaction = await TransactionModel.findOne({
      where: {
        sessionId: id,
      },
    });
    if (transaction) {
      if (status === 'SUCCESS') {
        transaction = transaction.update({ status, sessionSuccessTime: moment() });
      }
      else if (status === 'FAILED' || status === 'EXPIRED') {
        transaction = transaction.update({ status, sessionErrorTime: moment() });

      }
      else {
        transaction = transaction.update({ status });
      }
    }
    return transaction;
  }

};