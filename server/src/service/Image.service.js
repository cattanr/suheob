const { v4: uuidv4 } = require('uuid');
const { suheob: suheobDb } = require('../database');

let ImageModel;
module.exports = class ImageService {

  static init() {
    if (ImageModel === undefined) ImageModel = suheobDb.getModel('image');
  }

  static async findOrCreate(courseId, name, url) {
    ImageService.init();
    let image = await ImageModel.findOne({ where: { name } });
    // ajouter les includes pour vérifier l'appartenance
    if (image === null) {
      const imageObj = { id: uuidv4(), name, url, courseId };
      image = await ImageModel.create(imageObj);
    }
    return image;
  }

};