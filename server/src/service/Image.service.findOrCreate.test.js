const { v4: uuidv4 } = require('uuid');
const ImageService = require('./Image.service');

const { findOrCreate } = ImageService;
test('should return new image if image doesn\'t exist', async () => {
  const name = 'Robert';
  const url = 'https://cattanr.fr';
  const courseId = uuidv4();

  const image = await findOrCreate(courseId, name, url);

  expect(image.name).toEqual(name);
  expect(image.url).toEqual(url);
  expect(image.courseId).toEqual(courseId);
});
