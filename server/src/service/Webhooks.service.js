const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
const TransactionService = require('./Transaction.service');

module.exports = class WebhooksService {

  static async handle(req) {
    const webhookSecret = process.env.STRIPE_WEBHOOK_SECRET;
    const sig = req.headers['stripe-signature'];
    let event;
    try {
      event = stripe.webhooks.constructEvent(req.body, sig, webhookSecret);
    } catch (error) {
      return error;
    }
    let session;
    let transaction;
    let user;
    switch (event.type) {
      case 'checkout.session.async_payment_failed': // sofort
        session = event.data.object;
        TransactionService.updateStatusById(session.id, 'FAILED');
        // envoyer un email au client pour lui notifier le refus de paiement (en théorie)
        break;
      case 'checkout.session.async_payment_succeeded': // sofort
        break;
      case 'checkout.session.completed':
        session = event.data.object;
        if (session.payment_status === 'paid') {
          transaction = await TransactionService.updateStatusById(session.id, 'SUCCESS');
          if (transaction) {
            user = await transaction.getUser();
            if (user) {
              transaction.pack.forEach(courseId => {
                user.addCourse(courseId);
              });
            }
          }
        }
        break;
      case 'checkout.session.expired':
        session = event.data.object;
        TransactionService.updateStatusById(session.id, 'EXPIRED');
        break;
      default:
        break;
    }

  }

};