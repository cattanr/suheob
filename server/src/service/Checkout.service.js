const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
const { CourseService, TransactionService, UserService } = require('.');
const { InternalServerError, ForbiddenError } = require('../utils');
const { checkoutCreationJoiSchema } = require('../joiSchema');

module.exports = class CreateCheckoutService {

  static async create(data, keycloakId) {
    const user = await UserService.findOrCreate(keycloakId);
    if (user) {
      const { error } = checkoutCreationJoiSchema.validate(data);
      if (error !== undefined) return error.details[0].message;
      const { cart, email } = data;
      let amount = 0;
      const pack = [];
      const itemsArray = [];
      await Promise.each(cart, async product => {
        const course = await CourseService.getPublicCourse(product.id);
        pack.push(course.id);
        amount += course.price;
        itemsArray.push({
          price_data: {
            currency: 'eur',
            product_data: {
              name: course.title,
            },
            unit_amount: course.price * 100,
          },
          quantity: 1,
        });
      });
      if (amount === 0) {
        const transaction = await TransactionService.create(keycloakId, pack, 0, null);
        if (transaction === undefined) {
          throw new InternalServerError({ message: 'Failed to create transaction' });
        }
        transaction.pack.forEach(courseId => {
          user.addCourse(courseId);
        });
        transaction.update({ status: 'SUCCESS' });
        return { type: 'free', sessionId: null };
      }
      const session = await stripe.checkout.sessions.create({
        customer_email: email,
        line_items: itemsArray,
        payment_method_types: ['card'],
        billing_address_collection: 'auto',
        shipping_address_collection: {
          allowed_countries: ['FR'],
        },
        mode: 'payment',
        success_url: `${process.env.URL_CLIENT}/payment?status=success`,
        cancel_url: `${process.env.URL_CLIENT}/payment?status=error`,
      });
      const transaction = await TransactionService.create(keycloakId, pack, amount, session.id);
      if (transaction === undefined) {
        throw new InternalServerError({ message: 'Failed to create transaction' });
      }
      return { type: 'payment', sessionId: session.id };
    }
    throw new ForbiddenError('You are not allowed to access this ressource');
  }

};