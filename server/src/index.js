const express = require('express');
const Promise = require('bluebird');
const fileUpload = require('express-fileupload');
const path = require('path');
const cors = require('cors');
const { renderBackendError, Logger } = require('./utils');
const createRouter = require('./route/init_router');
const { DbsIniter } = require('./database/db_initer');
const { keycloackConfig, storeConfig, sessionConfig } = require('./config');

global.Promise = Promise;
global.serverUrl = process.env.URL_SERVER;
global.clientUrl = process.env.URL_CLIENT;

const app = express();

const store = storeConfig.init();
const keycloak = keycloackConfig.init(store);

app.use(express.urlencoded({ extended: true }));
app.use('/webhooks/5a9acadc-a4f9-447f-b0ae-5d0404d139f6', express.raw({ type: '*/*' }));
app.use(express.json());
app.use(sessionConfig.init(store));
app.use(keycloak.middleware());
app.use(cors());
app.use(fileUpload());
app.use(express.static(path.join(__dirname, 'public')));

createRouter(app, keycloak);
// eslint-disable-next-line no-unused-vars
app.use((err, _req, res, _next) => renderBackendError(res, err));

DbsIniter.init().then(() => {
  app.listen(process.env.PORT_SERVER, () => {
    Logger.info(`Listen on port : ${process.env.PORT_SERVER}`);
    Logger.info(`Server url : ${global.serverUrl}`);
  });
});