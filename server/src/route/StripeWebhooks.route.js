const controller = require('../controller');

module.exports = [{
  method: 'POST',
  path: '/webhooks/5a9acadc-a4f9-447f-b0ae-5d0404d139f6',
  validators: [],
  handler: controller.Webhooks.handle,
  authRequired: false,
}];