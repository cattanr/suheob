const express = require('express');
const Promise = require('bluebird');
const { renderOk } = require('../utils');
const route = require('./index');

module.exports = (app, keycloak, basePath = '') => {
  const router = express.Router();
  Object.keys(route)
    .map((key) => route[key])
    .forEach((rs) => rs.forEach((r) => {
      let middleware = (req, res, next) => next();
      if ('authRequired' in r && r.authRequired) middleware = keycloak.protect();
      router[r.method.toLowerCase()](basePath + r.path, middleware, (req, res, next) => {
        Promise.each(r.validators, (validator) => validator(req, res))
          .then(() => r.handler(req, res))
          .then((data) => renderOk(res, data))
          .catch(next);
      });
    }));
  app.use(router);
};
