const controller = require('../controller');

module.exports = [{
  method: 'POST',
  path: '/create_checkout_sessions',
  validators: [],
  handler: controller.Checkout.create,
  authRequired: true,
}];