const HealCheckRoute = require('./HealthCheck.route');
const CreateCheckoutSession = require('./CreateCheckoutSession.route');
const StripeWebhook = require('./StripeWebhooks.route');
const Course = require('./Course.route');
const User = require('./User.route');

module.exports = {
  HealCheckRoute,
  CreateCheckoutSession,
  StripeWebhook,
  Course,
  User,
};