const controller = require('../controller');

module.exports = [
  {
    method: 'GET',
    path: '/user/:userId/course/:courseId/image/:name',
    validators: [],
    handler: controller.User.getImageByName,
    authRequired: false,
  },
];
