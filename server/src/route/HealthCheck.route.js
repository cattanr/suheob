const controller = require('../controller');

module.exports = [
  {
    method: 'GET',
    path: '/health-check',
    validators: [],
    handler: controller.HealthCheck.check,
  },
];