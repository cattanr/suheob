import React from 'react';
import BackOfficeMenu from '../../../../../high_order_components/BackOfficeMenu';
import WithAuth from '../../../../../high_order_components/WithAuth';
import Chapter from '../../../../../components/builder/Chapter';
import SelectStep from '../../../../../components/builder/SelectStep';

class Build extends React.Component {

  static getInitialProps({query}) {
    return {...query};
  }

  render() {
    const { courseId } = this.props;
    return <>
    <div style={{ display: 'flex', justifyContent:'flex-start' }}>
      <div style={{ width:'100%', maxWidth:'1200px' }}>
        <div style={{ margin:'10px 0px' }}>
          <SelectStep courseid={courseId} option="build"/>
        </div>
        <Chapter courseid={courseId}/>
      </div>
    </div>
    </>;
  }

}
export default WithAuth(BackOfficeMenu(Build));