import React from 'react';
import { Divider, InputNumber, Button, Form, message } from 'antd';
import { getKeycloakInstance } from '@react-keycloak/ssr';
import Router from 'next/router';
import BackOfficeMenu from '../../../../../high_order_components/BackOfficeMenu';
import WithAuth from '../../../../../high_order_components/WithAuth';
import SelectStep from '../../../../../components/builder/SelectStep';
import CourseService from '../../../../../services/Course.service';
import { Pause } from '../../../../../utils';


class Pricing extends React.Component {

  constructor() {
    super();
    this.ref = React.createRef();
  }

  static getInitialProps({query}) {
    return { ...query };
  }

  async componentDidMount() {
    try {
      const { courseId } = this.props;
      const keycloakInstance = getKeycloakInstance();
      const course = await CourseService.getInstructorCourseById(keycloakInstance.token, courseId);
      if (course === null) Router.push('/instructor/courses');
      else if(course.price && this.ref.current !== null) this.ref.current.setFieldsValue({price: course.price});
    } catch (error) {
      message.error(error.message);
    }
  }

  async updateCourse(formData) {
    try {
      const { price } = formData;
      const { courseId } = this.props;
      const keycloakInstance = getKeycloakInstance();
      const response = await CourseService.updateCourseByBearer(keycloakInstance.token, courseId, { price } );
      if (response) {
        message.success(`Votre tarification a bien été enregistré`);
      }
      await Pause(1000);
      Router.push('/instructor/courses');
    } catch (error) {
      message.error(error.message);
    }
  }

  render() {
    const { courseId } = this.props;
    return <>
      <div style={{ width:'100%', maxWidth:'580px' }}>
        <div style={{ margin:'10px 0px' }}>
          <SelectStep courseid={courseId} option="pricing"/>
        </div>
        <div style={{ marginTop:'20px' }}>
          <h2>Tarification</h2>
        </div>
        <Divider/>
        <div>
          <h4>Tarif du cours</h4>
          <p>Choisissez le tarif de votre cours. Si vous voulez que votre cours soit gratuit, il suffit d'indiquer 0 dans le champs du prix.</p>
          <div style={{ width:'100%', display:'flex', justifyContent:'flex-start' }}>
          <Form 
            layout="vertical"
            ref={this.ref}
            onFinish={this.updateCourse.bind(this)}
          >
            <Form.Item name="price" 
                rules={[
                  { required: true, message:'Un prix est obligatoire' },
                  { type: 'number', max: 9999, message:'Votre prix doit être un nombre, maximum 9999' }
                ]} 
              >
              <InputNumber
                placeholder="19.99"
                addonAfter="€"
                style={{marginRight:'10px', maxWidth:'150px'}}
              />
            </Form.Item>
          </Form>
            <Button className="btn-black" onClick={() => this.ref.current.submit()} style={{ height:'32px', maxWidth:'150px', padding:'0' }}>Sauvegarder</Button>
          </div>
        </div>
      </div>
    </>;
  }

}
export default WithAuth(BackOfficeMenu(Pricing));