import React from 'react';
import BackOfficeMenu from '../../../../../high_order_components/BackOfficeMenu';
import WithAuth from '../../../../../high_order_components/WithAuth';
import { Course } from '../../../../../containers';
import SelectStep from '../../../../../components/builder/SelectStep';

class Basics extends React.Component {

  static getInitialProps({query}) {
    return { ...query };
  }

  render() {
    const { courseId } = this.props;
    return <>
      <div style={{margin:'10px 0px'}}>
        <SelectStep courseid={courseId} option="basics"/>
      </div>
      <div style={{borderBottom: '1px solid #d1d7dc', marginBottom:'20px'}}>
        <h2>Page d'accueil du cours</h2>
      </div>
      <Course.Form courseid={this.props.courseId}/>
    </>;
  }

}
export default WithAuth(BackOfficeMenu(Basics));