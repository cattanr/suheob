import React from 'react';
import Router from 'next/router';
import { Result, Button, message } from 'antd';
import { FormOutlined, LoadingOutlined } from '@ant-design/icons';
import { getKeycloakInstance } from '@react-keycloak/ssr';
import CourseService from '../../../../../services/Course.service';
import BackOfficeMenu from '../../../../../high_order_components/BackOfficeMenu';
import WithAuth from '../../../../../high_order_components/WithAuth';

class Review extends React.Component {

  constructor() {
    super();
    this.state = {
      review: {status: undefined, message: undefined},
    }
  }

  static getInitialProps({query}) {
    return {...query};
  }

  async componentDidMount() {
    try {
      const { courseId } = this.props;
      const keycloakInstance = getKeycloakInstance();
      const response = await CourseService.askReview(keycloakInstance.token, courseId);
      if (response) {
        this.setState({ review: response });
      }
    } catch (error) {
      message.error(error.message);
    }
  }


  formatResult(status) {
    const { review } = this.state;
    let content;
    const button = <Button className="btn-black" onClick={() => Router.push(`/instructor/courses`)} style={{ maxWidth:'270px'}}>Retour à mes cours</Button>;
    switch (status) {
      case 'DRAFT':
        content = <Result icon={<FormOutlined/>} title={review.message} extra={button}/>;
        break;
      case 'LIVE':
        content = <Result status="success" title={review.message} extra={button}/>;
        break;
      case 'IN_REVIEW':
        content = <Result icon={<LoadingOutlined/>} subTitle={review.message} extra={button}/>;
        break;
      default:
        content = <Result icon={<FormOutlined/>} title={review.message} extra={button}/>;
        break;
    }
    return content;
  }

  render() {
    const { review } = this.state;
    return <>
      { this.formatResult(review.status) }
    </>;
  }
}
export default WithAuth(BackOfficeMenu(Review));