import React from 'react';
import BackOfficeMenu from '../../high_order_components/BackOfficeMenu';
import WithAuth from '../../high_order_components/WithAuth';
import CourseService from '../../services/Course.service';
import InstructorCard from '../../components/course/InstructorCard';
import { Select, Empty, message } from 'antd';
import { getKeycloakInstance } from '@react-keycloak/ssr';
import Router from 'next/router';

const { Option } = Select;

class InstructorCourses extends React.Component {

  constructor() {
    super();
    this.state = {
      keycloakInstance: undefined,
      courses: undefined,
    };
  }

  async componentDidMount() {
    try {
      const keycloakInstance = getKeycloakInstance();
      const courses = await CourseService.getInstructorCourses(keycloakInstance.token);
      this.setState({ keycloakInstance, courses });
    } catch (error) {
      message.error(error.message);
    }
  }

  handleChange(value) {
    // trier les cours en fonction de value
  }

  async createCourse() {
    try {
      const course = await CourseService.createCourseByBearer(this.state.keycloakInstance.token);
      if (course) {
        Router.push(`/instructor/course/${course.id}/manage/basics`);
      }
    } catch (error) {
      message.error("Erreur lors de la création du cours, veuillez réessayer ultérieurement");
    }
  }

  render() {
    const { courses } = this.state;
    return <>
      <div>
        <div className="courses-header">
          <h1 style={{ margin:'0' }}>Cours</h1>
          <button className="button-primary" onClick={ () => this.createCourse() } style={{ width:'140px' }}>Nouveau cours</button>
        </div>
        <div className="courses-sort-row">
          <Select defaultValue="more-recent" onChange={this.handleChange} style={{ width:'170px'}}>
            <Option value="more-recent">Les plus récents</Option>
            <Option value="oldest">Le plus ancien</Option>
            <Option value="a-z">A-Z</Option>
          </Select>
        </div>
      </div>
      <div>
        <div style={{ display:'flex', justifyContent:'center' }}>
          <div className="courses-list">
            { courses && courses.length !== 0 ?
              courses.map(course => {
                return <InstructorCard key={course.id} course={course}/>
              })
              : <div><Empty image={Empty.PRESENTED_IMAGE_SIMPLE} /></div>
            }
          </div>
        </div>
      </div>
    </>;
  }

}
export default WithAuth(BackOfficeMenu(InstructorCourses));