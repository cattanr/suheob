import cookie from 'cookie';
import getConfig from 'next/config';
import { SSRKeycloakProvider, SSRCookies } from '@react-keycloak/ssr';
import Auth from '../components/auth/';
import 'antd/dist/antd.min.css';
import '../public/css/Index.css';
import '../public/css/Carousel.css';
import '../public/css/Teaching.css';
import '../public/css/Menu.css';
import '../public/css/NavBar.css';
import '../public/css/Card.css';
import '../public/css/Product.css';
import '../public/css/Cart.css';
import '../public/css/Instructor.css';
import '../public/css/Builder.css';



const { publicRuntimeConfig } = getConfig();

const keycloakConfig = {
  realm:'suheob',
  url: publicRuntimeConfig.KEYCLOAK_URL,
  clientId: 'client'
};

const initOptions = {
  onLoad: 'check-sso',
};

function MyApp({ Component, pageProps, cookies }) {
  return (
    <SSRKeycloakProvider keycloakConfig={keycloakConfig}
      LoadingComponent={<Auth.LoadingPage/>}
      persistor={SSRCookies(cookies)} initOptions={initOptions}>
      <Component {...pageProps}/>
    </SSRKeycloakProvider>
  );
}
 
function parseCookies(req) {
  if (!req || !req.headers) {
    return {};
  }
  return cookie.parse(req.headers.cookie || '');
}

MyApp.getInitialProps = async ({ Component, ctx }) => {
 
  const pageProps = Component.getInitialProps
    ? {...(await Component.getInitialProps(ctx)),
      cookies: parseCookies(ctx?.req)}
    : {cookies: parseCookies(ctx?.req)};
  return { pageProps };
};
export default MyApp;

