import React from 'react';
import Menu from '../../../high_order_components/Menu';
import WithAuth from '../../../high_order_components/WithAuth';
import TeacherHeader from '../../../components/navigation/TeacherHeader';

class Wishlist extends React.Component {

  render() {
    return <>
      <TeacherHeader activeTab="wishlist"/>
      <div>WIP</div>
    </>;
  }

}

export default WithAuth(Menu(Wishlist));