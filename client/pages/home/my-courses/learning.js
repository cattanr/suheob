import React from 'react';
import { getKeycloakInstance } from '@react-keycloak/ssr';
import { Empty, message } from 'antd';
import Menu from '../../../high_order_components/Menu';
import WithAuth from '../../../high_order_components/WithAuth';
import TeacherHeader from '../../../components/navigation/TeacherHeader';
import MyCourseCard from '../../../components/course/MyCourseCard';
import CourseService from '../../../services/Course.service';

class Learning extends React.Component {

  constructor() {
    super();
    this.state = {
      courses: null,
    };
  }

  async componentDidMount() {
    try {
      const keycloakInstance = getKeycloakInstance();
      const courses = await CourseService.getMyCourses(keycloakInstance.token);
      if (courses) {
        this.setState({ courses });
      }
    } catch (error) {
      message.error(error.message);
    }
  }

  render() {
    const { courses } = this.state;
    return <>
      <TeacherHeader activeTab='all-courses'/>
      <div className="card-wrapper">
        { courses ? 
          courses.map((course, index) =>
          <div className="card-learning" key={index}>
            <MyCourseCard content={course}/>
          </div>
          )
          : <div><Empty image={Empty.PRESENTED_IMAGE_SIMPLE} /></div>
        } 
      </div>
    </>;
  }

}

export default WithAuth(Menu(Learning));