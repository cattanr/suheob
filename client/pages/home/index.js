import React from 'react';
import Router from 'next/router';
import WithAuth from '../../high_order_components/WithAuth';

class homeIndex extends React.Component {

  componentDidMount() {
    Router.push('/');
  }

  render() {
    return <></>;
  }

}

export default WithAuth(homeIndex);