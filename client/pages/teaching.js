import React from 'react';
import Link from 'next/link';
import Menu from '../high_order_components/Menu';

class Teaching extends React.Component {

  render() {
    return <>
      <div>
        <div style={{ margin:'-24px -24px 0px -24px', backgroundColor:'#EDEDED'}}>
          <img className="billboard-img" alt="teacher-billboard" src="/img/teacher-billboard.jpg" width="650" height="416"/>
        </div>
        <div style={{padding:'24px 0px', width:'100%', display:'flex', flexDirection:'column'}}>
          <div style={{display:'flex', justifyContent:'center'}}>
            <h1 style={{fontWeight:'700', fontSize:'calc(24px + 1vw)'}}>Ayez un impact global</h1>
          </div>
          <div style={{display:'flex', justifyContent:'center'}}>
            <p style={{fontSize:'calc(12px + 1vw)', textAlign:'center'}}>Construisez votre cours en ligne et monétisez votre expertise en partageant votre savoir partout dans le monde.</p>
          </div>
          <div style={{display:'flex', justifyContent:'center'}}>
            <Link href="/instructor/courses">
              <a style={{ width:'100%', maxWidth:'550px'}}>
                <button className="btn-black"><span>Ayez un impact global</span></button>
              </a>
            </Link>
          </div>
        </div>
      </div>
      <div style={{padding:'24px 0px'}}>
        <div style={{display:'flex', justifyContent:'center'}}>
          <h2 style={{fontSize:'calc(18px + 1vw)', fontWeight:'700'}}>Il y a tant de raisons de se lancer</h2>
        </div>
        <div className="props-container">
          <div className="value-props">
            <div style={{ marginBottom:'14px' }}>
              <img alt="prop-teach" src="/img/value-prop-teach.jpg" width="240" height="240" style={{width:'100px', height:'100px'}}/>
            </div>
            <div className="props-title">
              Créez des cours qui vous ressemblent
            </div>
            <div className="props-text">
              Publiez le cours que vous voulez, comme vous voulez et gardez toujours le contrôle sur votre propre contenu.
            </div>
          </div>
          <div className="value-props">
            <div style={{ marginBottom:'14px' }}>
              <img alt="prop-teach" src="/img/value-prop-inspire.jpg" width="240" height="240" style={{width:'100px', height:'100px'}}/>
            </div>
            <div className="props-title">
              Inspirez les participants
            </div>
            <div className="props-text">
              Enseignez ce que vous savez et aidez les participants à explorer leurs intérêts, à acquérir de nouvelles compétences et à faire progresser leur carrière.
            </div>
          </div>
          <div className="value-props">
            <div style={{ marginBottom:'14px' }}>
              <img alt="prop-teach" src="/img/value-prop-get-rewarded.jpg" width="240" height="240" style={{width:'100px', height:'100px'}}/>
            </div>
            <div className="props-title">
              Soyez récompensé
            </div>
            <div className="props-text">
              Développez votre réseau professionnel et votre expertise, gagnez de l'argent pour chaque inscription payante.
            </div>
          </div>
        </div>
      </div>
    </>;
  }

}
export default Menu(Teaching);