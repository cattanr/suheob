import React from 'react';
import PaymentResult from '../components/checkout/PaymentResult';

class Payment extends React.Component {

  static getInitialProps({query}) {
    return { ...query };
  }

  render() {
    const { status } = this.props;
    return <PaymentResult status={status}/>;
  }

}
export default Payment;