import React from 'react';
import { getKeycloakInstance } from '@react-keycloak/ssr';
import { Pagination, Empty, message } from 'antd';
import { Menu, WithAuth } from '../../../../../high_order_components';
import CourseService from '../../../../../services/Course.service';
import courseBlocks from '../../../../../components/courseBlocks';
import Router from 'next/router';

class ChapterIndex extends React.Component {

  constructor() {
    super();
    this.state = {
      chapters: null
    }
  }

  static getInitialProps({query}) {
    return {... query };
  }

  async componentDidMount() {
    try {
      const { courseSlug } = this.props;
      const keycloakInstance = getKeycloakInstance();
      const response = await CourseService.getChapters(keycloakInstance.token, courseSlug);
      const { chapters } = response;
      if (chapters.length !== 0) this.setState({ chapters });
    } catch (error) {
      message.error(error.message);
    }
  }

  pageChange(page) {
    const { courseSlug } = this.props;
    Router.push(`/course/${courseSlug}/learn/chapter/${page}`);
  }

  render() {
    const { chapterIndex } = this.props;
    const { chapters } = this.state;
    let blockArray = <></>;
    if (chapters) {
      const { blocks } = chapters[chapterIndex-1];
      blockArray = blocks.map(block => {
        const { id, type, content } = block;
        const DinamicBlock = courseBlocks[type];
        return <DinamicBlock key={id} content={content}/>
      });
    }
    return <>
      {chapters ?
        <>
          <div>
            <div className="product-container" style={{ justifyContent:'center' }}>
              <div style={{ width:'100%', maxWidth:'950px' }}>
                <h1 style={{margin:'0', fontFamily:'SuisseWorks', fontSize:'32px'}}>Chapitre {chapterIndex}: {chapters[chapterIndex-1].title}</h1>
              </div>
            </div>
            <div className="blocks-container">
              <div style={{ width:'100%', maxWidth:'950px' }}>
                {blockArray}
                <div>
                  <Pagination
                    simple
                    defaultCurrent={1}
                    pageSize={1}
                    current={parseInt(chapterIndex)}
                    total={chapters.length}
                    onChange={this.pageChange.bind(this)}
                    style={{minWidth:'159px'}}
                  />
                </div>
              </div>
            </div>
          </div>
        </>
      : <span><Empty image={Empty.PRESENTED_IMAGE_SIMPLE} /></span>}
    </>;
  }

}
export default WithAuth(Menu(ChapterIndex));