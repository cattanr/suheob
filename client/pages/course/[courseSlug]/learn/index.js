import React from 'react';
import { getKeycloakInstance } from '@react-keycloak/ssr';
import { Rate, Empty, message } from 'antd';
import CourseService from '../../../../services/Course.service';
import { Menu, WithAuth } from '../../../../high_order_components';
import ChapterCard from '../../../../components/course/ChapterCard';
import Link from 'next/link';

class Learn extends React.Component {

  static getInitialProps({query}) {
    return {... query };
  }

  constructor() {
    super();
    this.state = {
      course: undefined,
    }
  }

  async componentDidMount() {
    try {
      const keycloakInstance = getKeycloakInstance();
      const { courseSlug } = this.props;
      const course = await CourseService.getMyCourse(keycloakInstance.token, courseSlug);
      if (course) {
        this.setState({ course });
      }
    } catch (error) {
      message.error(error.message);
    }
  }

  render() {
    const { course } = this.state;
    const { courseSlug } = this.props;
    let config;
    if (course) config = JSON.parse(course.config);
    return <>
      { course ?
        <>
          <div className="product-container">
            <div className="main-content">
              <div className="flex-container">
                <img alt="course-image" src={course.main_image} style={{ width:'100%', height:'auto', maxWidth:'450px', maxHeight:'250px' }}/>
              </div>
              <div className="flex-container text-container">
                <div style={{ display:'flex', flexDirection:'column', justifyContent:'space-between', width:'100%', maxWidth:'450px'}}>
                  <h1>{course.title}</h1>
                  <span style={{color:'#B4690E'}}>5
                    <Rate disabled allowHalf defaultValue="5" style={{ color:'#E59719', fontSize:'15px', paddingLeft:'8px'}}/>
                  </span>
                  <span>créé par { course.creator }</span>
                  <Link href={`/course/${courseSlug}/learn/chapter/1`}>
                    <a>
                      <button className="button-primary" style={{ textAlign:'center', marginTop:'5px'}}>Accéder au cours</button>
                    </a>
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className="product-extra-container">
            <div className="secondary-content">
              {config.chapters.map((chapterId, index) => {
                const found = course.chapters.find(element => element.id === chapterId);
                if (found) return <ChapterCard key={index} courseid={courseSlug} content={found} chapternumber={index + 1}/>
              })}
            </div>
          </div>
          <div className="product-extra-container">
            <div className="secondary-content">
              <h2>Description</h2>
              <p>{course.description}</p>
            </div>
          </div>
        </>
        : <div><Empty image={Empty.PRESENTED_IMAGE_SIMPLE} /></div>
        }
    </>;
  }

}
export default WithAuth(Menu(Learn));