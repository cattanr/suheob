import React from 'react';
import { Rate } from 'antd';
import { Menu } from '../../../high_order_components';
import { CourseService } from '../../../services';
import { getKeycloakInstance } from '@react-keycloak/ssr';
import Link from 'next/link';
import { Empty, message } from 'antd';

class CourseIndex extends React.Component {

  constructor() {
    super();
    this.state = {
      course: undefined,
      alreadyBought: false,
    }
  }

  static getInitialProps({query}) {
    return { ...query };
  }

  async componentDidMount() {
    try {
      const keycloakInstance = getKeycloakInstance();
      let course;
      const { courseSlug } = this.props;
      let alreadyBought = false;
      if (keycloakInstance.authenticated) {
        course = await CourseService.getCourse(keycloakInstance.token, courseSlug);
        if (course.users.length !== 0) alreadyBought = true;
      }
      else course = await CourseService.getPublicCourse(courseSlug);
      if (course) {
        this.setState({ course, alreadyBought });
      }
    } catch (error) {
      message.error(error.message);
    }
  }

  addToCart(item) {
    const { cart, onCartUpdate } = this.props;
    let newCart = [...cart];
    if(cart.length === 0) {
      newCart.push(item);
    }
    else {
      let alreadyOnCart = false;
      cart.forEach(element => {
        if (element.id === item.id) alreadyOnCart = true
      });
      if (!alreadyOnCart) newCart.push(item);
    }
    localStorage.setItem('cart', JSON.stringify(newCart));
    onCartUpdate(newCart);
  }

  render() {
    const { course, alreadyBought } = this.state;
    return <>
      { course ?
        <>
          <div className="product-container">
            <div style={{ width:'100%', maxWidth:'800px' }}>
              <img alt="course-image" src={course.main_image} style={{ width:'100%', heigth:'auto', maxHeight:'450px' }}/>
              <div className="product">
                <h1>{course.title}</h1>
              </div>
              <div className="product-rate">
                <span style={{color:'#B4690E'}}>5
                  <Rate disabled allowHalf defaultValue="5" style={{ color:'#E59719', fontSize:'15px', paddingLeft:'8px'}}/>
                </span>
              </div>
              <div className="product">
                <span>créé par {course.creator}</span>
              </div>
              <div className="product">
                <h3>{ course.price !== 0 ? `${course.price} €` : 'Gratuit' }</h3>
              </div>
              { alreadyBought ? 
                <Link href={`/course/${course.id}/learn`}><a><button className="button-primary">Accéder au cours</button></a></Link>
                : <button className="button-primary" onClick={() => this.addToCart(course)}>Ajouter au panier</button>
               }
            </div>
          </div>
          <div className="product-extra-container">
            <div style={{ width:'100%', maxWidth:'800px' }}>
              <h2>Description</h2>
              <p>{course.description}</p>
            </div>
          </div>
        </>
        : <div><Empty image={Empty.PRESENTED_IMAGE_SIMPLE} /></div>
        }
    </>;
  }

}

export default Menu(CourseIndex);