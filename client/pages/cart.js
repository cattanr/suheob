import React from 'react';
import { Divider } from 'antd';
import CartContent from '../components/cart/CartContent';
import Menu from '../high_order_components/Menu';
import WithAuth from '../high_order_components/WithAuth';
import CheckoutForm from '../components/checkout/CheckoutForm';

class Cart extends React.Component {

  render() {
    const { cart, onCartUpdate } = this.props;
    let total = cart.reduce((acc, current) => acc + current.price, 0);
    return <>
      <div style={{ display:'flex', justifyContent:'center' }}>
        <div style={{ width:'100%', maxWidth:'600px' }}>
          {cart.length !== 0 ? <>
            <div style={{fontSize:'18px', paddingBottom:'10px'}}>
              Total: <span style={{fontSize:'36px'}}><strong>{total}€</strong></span>
            </div>
            <CheckoutForm cart={cart} />
            <Divider/></>
            : <></>
          }
          <div>
            <h3 style={{ fontSize:'18px', fontWeight:'400', lineHeight:'1.2', fontFamily:'unset' }}>{cart.length} cours dans votre panier</h3>
            <CartContent content={cart} hover={false} onUpdate={onCartUpdate}></CartContent>
          </div>
        </div>
      </div>
    </>;
  }

}
export default WithAuth(Menu(Cart));