import React from 'react';
import { Menu } from '../high_order_components';
import CourseCarousel from '../components/course/CourseCarousel';
import Link from 'next/link';
import { message } from 'antd';
import { getKeycloakInstance } from '@react-keycloak/ssr';
import CourseService from '../services/Course.service';

class Index extends React.Component  {

  constructor() {
    super();
    this.state = {
      courses: undefined,
    }
  }

  async componentDidMount() {
    try {
      let courses;
      const keycloakInstance = getKeycloakInstance();
      if (keycloakInstance.authenticated) courses = await CourseService.getLastOnlineCourses(keycloakInstance.token);
      else courses = await CourseService.getPublicLastOnlineCourses();
      if (courses && courses.length !== 0) this.setState({ courses });
    } catch (error) {
      message.error(error.message);
    }
  }

  render() {
    const { courses } = this.state;
    return <>
      <div style={{ margin:'-24px -24px 0px -24px', backgroundColor:'#F8F9FB'}}>
        <img className="billboard-img" alt="teacher-billboard" src="/img/landing-billboard.jpg" width="1340" height="400"/>
      </div>
      <div style={{ marginTop:'20px' }}>
        <h1>Il y a toujours quelque chose de nouveau à apprendre</h1>
      </div>
      <div style={{ marginTop:'20px' }}>
        <p>
          Avec des cours ajoutés régulièrement à notre catalogue, vous bénéficiez toujours des compétences en vogue.
        </p>
      </div>
      <div style={{ marginTop:'20px' }}>
        <h2>Derniers cours mis en ligne</h2>
      </div>
      <div className="carousel-container" style={{ marginTop:'20px' }}>
        <CourseCarousel courses={courses}/>
      </div>
      <div className="teacher-container">
        <img className="teacher-img" alt="teacher" src="/img/teacher.jpg" width="600" height="450"/>
        <div className="teacher-text">
          <h2>Devenir formateur</h2>
          <p>Nos formateurs du monde entier donnent des cours à des millions de participants. Nous vous offrons les outils et les compétences nécessaires pour enseigner ce que vous aimez.</p>
          <Link href="/teaching">
          <a>
            <button className="btn-black">Commencez à enseigner</button>
          </a>
          </Link>
        </div>
      </div>
    </>;
  }

}

export default Menu(Index);