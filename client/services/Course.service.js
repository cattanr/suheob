import axios from 'axios';

export default class CourseService {

  static async createCourseByBearer(token, data = {}) {
    const url = `${process.env.NEXT_PUBLIC_URL_SERVER}/courses`;
    const response = await axios({
      method: 'post',
      url,
      data,
      headers: { Authorization: `Bearer ${token}` }
    });
    return response.data;
  }

  static async updateCourseByBearer(token, id, data) {
    const url = `${process.env.NEXT_PUBLIC_URL_SERVER}/instructor/course/${id}`;
    const response =  await axios({
      method: 'put',
      url,
      data,
      headers: { Authorization: `Bearer ${token}` }
    });
    return response.data;
  }

  static async getInstructorCourseById(token, id) {
    const url = `${process.env.NEXT_PUBLIC_URL_SERVER}/instructor/course/${id}`;
    const response =  await axios({
      method: 'get',
      url,
      headers: { Authorization: `Bearer ${token}` }
    });
    return response.data;
  }

  static async createChapters(token, courseId, data) {
    const url = `${process.env.NEXT_PUBLIC_URL_SERVER}/course/${courseId}/chapters`;
    const response = await axios({
      method: 'post',
      url,
      data,
      headers: { Authorization: `Bearer ${token}` }
    });
    return response;
  }

  static async getChapters(token, courseId) {
    const url = `${process.env.NEXT_PUBLIC_URL_SERVER}/course/${courseId}/chapters`;
    const response = await axios({
      method: 'get',
      url,
      headers: { Authorization: `Bearer ${token}` }
    });
    return response.data;
  }

  static async removeChapter(token, courseId, chapterId) {
    const url = `${process.env.NEXT_PUBLIC_URL_SERVER}/course/${courseId}/chapter/${chapterId}`;
    const response = await axios({
      method: 'delete',
      url,
      headers: { Authorization: `Bearer ${token}` }
    });
    return response.data;
  }

  static async askReview(token, courseId) {
    const url = `${process.env.NEXT_PUBLIC_URL_SERVER}/course/${courseId}/review`;
    const response = await axios({
      method: 'get',
      url,
      headers: { Authorization: `Bearer ${token}` }
    });
    return response.data;
  }

  static async getInstructorCourses(token) {
    const url = `${process.env.NEXT_PUBLIC_URL_SERVER}/instructor/courses`;
    const response = await axios({
      method: 'get',
      url,
      headers: { Authorization: `Bearer ${token}` }
    })
    return response.data;
  }

  static async getPublicCourse(id) {
    const url = `${process.env.NEXT_PUBLIC_URL_SERVER}/course/${id}`;
    const response = await axios({
      method: 'get',
      url
    });
    return response.data;
  }

  static async getCourse(token, id) {
    const url = `${process.env.NEXT_PUBLIC_URL_SERVER}/user/course/${id}`;
    const response = await axios({
      method: 'get',
      url,
      headers: { Authorization: `Bearer ${token}` }
    });
    return response.data;
  }

  static async getPublicLastOnlineCourses() {
    const url = `${process.env.NEXT_PUBLIC_URL_SERVER}/courses`;
    const response = await axios({
      method: 'get',
      url
    });
    return response.data;
  }

  static async getLastOnlineCourses(token) {
    const url = `${process.env.NEXT_PUBLIC_URL_SERVER}/user/courses`;
    const response = await axios({
      method: 'get',
      url,
      headers: { Authorization: `Bearer ${token}` }
    });
    return response.data;
  }

  static async getMyCourse(token, id) {
    const url = `${process.env.NEXT_PUBLIC_URL_SERVER}/my-course/${id}`;
    const response = await axios({
      method: 'get',
      url,
      headers: { Authorization: `Bearer ${token}` }
    })
    return response.data;
  }

  static async getMyCourses(token) {
    const url = `${process.env.NEXT_PUBLIC_URL_SERVER}/my-courses`;
    const response = await axios({
      method: 'get',
      url,
      headers: { Authorization: `Bearer ${token}` }
    })
    return response.data;
  }

}