module.exports = {
  publicRuntimeConfig: {
    KEYCLOAK_URL: process.env.KEYCLOAK_URL,
  },
  webpack5: false,
};