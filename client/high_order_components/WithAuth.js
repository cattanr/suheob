import React from 'react';
import { getKeycloakInstance } from '@react-keycloak/ssr';

function WithAuth(WrappedComponent) {
  return class extends React.Component {

    static async getInitialProps(ctx) {
      const props = WrappedComponent.getInitialProps
        ? {...(await WrappedComponent.getInitialProps(ctx)) }
        : {};
      return props;
    }

    componentDidMount() {
      const keycloakInstance = getKeycloakInstance();
      if (!keycloakInstance?.authenticated) keycloakInstance?.login();
    }

    componentDidUpdate() {
      const keycloakInstance = getKeycloakInstance();
      if (!keycloakInstance?.authenticated) keycloakInstance?.login();
    }
    
    render() {
      const keycloakInstance = getKeycloakInstance();
      return <>
        { keycloakInstance?.authenticated && 
        (<WrappedComponent {...this.props}/>)
        }
      </>;
    }

  };
}

export default WithAuth;