import React from 'react';
import { Layout  } from 'antd';
import { Navigation} from "../components";
import Head from 'next/head';


const { Footer, Content } = Layout;

function Menu(WrappedComponent, openOrSelected = []) {
  return class extends React.Component {

    constructor() {
      super();
      this.state = {
        visibleModal: false,
        cart: []
      };
    }

    static async getInitialProps(ctx) {
      const props = WrappedComponent.getInitialProps
        ? {...(await WrappedComponent.getInitialProps(ctx)) }
        : {};
      return props;
    }

    componentDidMount() {
      this.setState({ cart: JSON.parse(localStorage.getItem('cart')) || [] });
    }
    
    render() {
      return <>
        <Navigation.NavBar openOrSelected={openOrSelected} cart={this.state.cart} onUpdate={(cart) => this.setState({cart: cart})}/>
        <Layout>
          <Content style={{ padding: '80px 24px 24px 24px', backgroundColor:'white', minHeight: 'calc(100vh - 60px)'}}>
            <WrappedComponent {...this.props} cart={this.state.cart} onCartUpdate={(cart) => this.setState({cart: cart})}/>
          </Content>
          <Footer style={{ textAlign: 'center', paddingTop: '20px', backgroundColor:'#1C1D1F', color:'white', height:'60px'}}>
            Suheob ©2021
          </Footer>
          <Navigation.InformationModal 
            visible={this.state.visibleModal} 
            onOk={() => this.setState({visibleModal: false})}
            onClick={() => this.setState({visibleModal: true})}/>
        </Layout>
      </>;
    }

  };
}

export default Menu;