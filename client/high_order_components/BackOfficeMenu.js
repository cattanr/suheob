import React from 'react';
import { Layout  } from 'antd';
import { Navigation} from "../components";

const { BackOfficeNav } = Navigation;
const { Footer, Content } = Layout;

function BackOfficeMenu(WrappedComponent, openOrSelected = []) {
  return class extends React.Component {

    constructor() {
      super();
      this.state = {
        visibleModal: false,
      };
    }

    static async getInitialProps(ctx) {
      const props = WrappedComponent.getInitialProps
        ? {...(await WrappedComponent.getInitialProps(ctx)) }
        : {};
      return props;
    }
    
    render() {
      return <>
        <Layout style={{ minHeight: '100vh' }}>
          <BackOfficeNav openOrSelected={openOrSelected}/>
          <Layout className="site-layout">
            <Content style={{ padding: '55px 24px 24px 24px', backgroundColor:'white', minHeight: 'calc(100vh - 60px)'}}>
              <WrappedComponent {...this.props}/>
            </Content>
            <Footer style={{ textAlign: 'center', paddingTop: '20px', backgroundColor:'#1C1D1F', color:'white', height:'60px'}}>
              Suheob ©2021
            </Footer>
            <Navigation.InformationModal
              visible={this.state.visibleModal} 
              onOk={() => this.setState({visibleModal: false})}
              onClick={() => this.setState({visibleModal: true})}/>
          </Layout>
        </Layout>
      </>;
    }

  };
}

export default BackOfficeMenu;