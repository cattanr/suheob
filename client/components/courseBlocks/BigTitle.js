import React from 'react';

class BigTitle extends React.Component {

  render() {
    const { content } = this.props;
    return <>
      <div className="course-block">
        <h1>{content}</h1>
      </div>
    </>;
  }

}
export default BigTitle;