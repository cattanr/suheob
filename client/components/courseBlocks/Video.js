import React from 'react';

class Video extends React.Component {

  render() {
    const { content } = this.props;
    return <>
      <div className="iframe-container course-block">
        <iframe 
          src={content} 
          frameBorder="0" 
          sandbox="allow-scripts allow-popups allow-top-navigation-by-user-activation allow-forms allow-same-origin" 
          allowFullScreen
        />      
      </div>
    </>;
  }

}
export default Video;