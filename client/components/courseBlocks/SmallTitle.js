import React from 'react';

class SmallTitle extends React.Component {

  render() {
    const { content } = this.props;
    return <>
      <div className="course-block">
        <h3>{content}</h3>    
      </div>
    </>;
  }

}
export default SmallTitle;