import React from 'react';


class Text extends React.Component {

  render() {
    const { content } = this.props;
    return <>
      <div className="course-block">
        <span>{content}</span>  
      </div>
    </>;
  }

}
export default Text;