import React from 'react';

class MediumTitle extends React.Component {

  render() {
    const { content } = this.props;
    return <>
      <div className="course-block">
        <h2>{content}</h2>
      </div>
    </>;
  }

}
export default MediumTitle;