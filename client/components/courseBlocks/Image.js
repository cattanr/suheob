import React from 'react';

class Image extends React.Component {

  render() {
    const { content } = this.props;
    return <>
      <div className="course-block">
        <img alt="course-image" src={content}/>
      </div>
    </>;
  }

}
export default Image;