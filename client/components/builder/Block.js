import React from 'react';
import BlockModal from './BlockModal';
import blocks from '../builderBlocks';

class Block extends React.Component {

  constructor() {
    super();
    this.state = {
      modalVisible: false,
    };
  }

  hideOnHover(element) {
    element.style.display = 'none';
  }
  
  displayOnHover(element) {
    element.style.display = 'inline-flex';
  }

  addNewBlock(previousBlock, id, type) {
    const { addBlock } = this.props;
    addBlock(previousBlock, id, type);
    this.setState({modalVisible: false});
  }

  updateModalVisibility(visible) {
    this.setState({modalVisible: visible});
  }

  render() {
    const { modalVisible } = this.state;
    const { courseid, data, onUpdate, onRemove } = this.props;
    const { id:blockId, type } = data;
    const DinamicBlock = blocks[type];
    return <>
      <div 
        onPointerDown={() => this.displayOnHover(document.getElementById(`${blockId}`))} 
        onMouseEnter={() => this.displayOnHover(document.getElementById(`${blockId}`))} 
        onMouseLeave={() => this.hideOnHover(document.getElementById(`${blockId}`))} 
        style={{ display: 'flex', justifyContent:'flex-start', alignItems:'flex-start', width:'inherit', minHeight:'28px', position:'relative', marginBottom:'5px'}}
      >
        { DinamicBlock ? 
          <DinamicBlock courseid={courseid} data={data} onRemove={(id) => onRemove(id)} onUpdate={(block) => onUpdate(block)} onChangeModalVisible={(visible) => this.updateModalVisibility(visible)}/>
          : <></>
        }
        <BlockModal visible={modalVisible} addBlock={(newBlockId, type) => this.addNewBlock(data, newBlockId, type)} onCancel={() => this.updateModalVisibility(false)}/>
      </div>
    </>;
  }

}
export default Block;