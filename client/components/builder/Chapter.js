import React from 'react';
import { v4 as uuidv4 } from 'uuid';
import Router from 'next/router';
import { Pagination, Button, Input, message } from 'antd';
import Block from './Block';
import CourseService from '../../services/Course.service';
import { getKeycloakInstance } from '@react-keycloak/ssr';

class Chapter extends React.Component {

  constructor() {
    super();
    this.state = {
      chapters: [
        { id: uuidv4(), title:'', blocks: [{id: uuidv4(), type:'Text', content:''}]},
      ],
      currentChapter: 1,
      saveDisabled: true,
    };
  }

  static getInitialProps({query}) {
    return {...query};
  }

  async componentDidMount() {
    try {
      const { courseid } = this.props;
      const keycloakInstance = getKeycloakInstance();
      const response = await CourseService.getChapters(keycloakInstance.token, courseid);
      const { chapters } = response;
      if (chapters.length !== 0) this.setState({ chapters: chapters});
      else this.setState({chapters: [{ id: uuidv4(), title:'', blocks: [{id: uuidv4(), type:'Text', content:''}]}]});
    } catch (error) {
      message.error(error.message);
    }
  }

  addNewBlock(previousBlock, id, type) {
    const { chapters, currentChapter } = this.state;
    const { blocks } = chapters[currentChapter-1];
    blocks.forEach((block, index) => {
      if(block.id === previousBlock.id) {
        blocks.splice(index+1, 0, {id, type, content:''});
        this.setState({chapters, saveDisabled: false});
      }
    });
  }

  removeBlockById(id) {
    const { chapters, currentChapter } = this.state;
    const { blocks } = chapters[currentChapter-1];
    blocks.forEach((block, index) => {
      if(blocks.length !== 1 && block.id === id) {
        blocks.splice(index, 1);
        this.setState({chapters, saveDisabled: false});
      }
    });
  }

  updateContent(blockToUpdate) {
    const { chapters, currentChapter } = this.state;
    const { blocks } = chapters[currentChapter-1];
    blocks.forEach((block) => {
      if(block.id === blockToUpdate.id) {
        block.content = blockToUpdate.content;
        this.setState({chapters, saveDisabled: false});
      }
    });
  }

  addChapter() {
    const { chapters, currentChapter } = this.state;
    chapters.push({ id: uuidv4(), title:'', blocks: [{id: uuidv4(), type:'Text', content:''}]});
    this.setState({ chapters, currentChapter: currentChapter+1, saveDisabled: false });
  }

  async removeChapter() {
    try {
      const { chapters, currentChapter } = this.state;
      const { courseid } = this.props;
      const keycloakInstance = getKeycloakInstance();
      const chapter = chapters.pop();
      const course = await CourseService.removeChapter(keycloakInstance.token, courseid, chapter.id);
      if (course) {
        this.setState({ chapters, currentChapter: currentChapter-1, saveDisabled: false });
      }
    } catch (error) {
      message.error(error.message);
    }
  }

  pageChange(page) {
    this.setState({currentChapter: page});
  }

  saveCourse() {
    const { courseid } = this.props;
    const { chapters } = this.state;
    const keycloakInstance = getKeycloakInstance();
    CourseService.createChapters(keycloakInstance.token, courseid, {chapters});
    this.setState({saveDisabled: true});
  }

  saveAndExit() {
    const { courseid } = this.props;
    const { saveDisabled } = this.state;
    if (!saveDisabled) this.saveCourse();
    Router.push(`/instructor/course/${courseid}/manage/pricing`)
  }

  handleChapterTitleInput({target: { value }}) {
    const { chapters, currentChapter } = this.state;
    chapters[currentChapter-1].title = value;
    this.setState({chapters, saveDisabled: false});
  }

  render() {
    const { courseid } = this.props;
    const { chapters, currentChapter, saveDisabled } = this.state;
    const { blocks } = chapters[currentChapter-1];
    const blockArray = blocks.map((block, index) => {
      return <Block
        key={index}
        courseid={courseid}
        data={block}
        addBlock={(previousBlock, id, type) => this.addNewBlock(previousBlock, id, type)} 
        onRemove={(id) => this.removeBlockById(id)}
        onUpdate={(block) => this.updateContent(block)}/>;
    });
    const chapterFooter = () => {
      const pagination = 
      <Pagination simple defaultCurrent={1} pageSize={1} current={parseInt(currentChapter)} total={chapters.length} onChange={this.pageChange.bind(this)} style={{minWidth:'159px'}}/>;
      const nextChapterButton = <button className="button-primary" onClick={() => this.addChapter()} style={{marginBottom:'10px', borderRadius:'2px'}}>Ajouter un chapitre</button>;
      const removeChapterButton = <button className="btn-black" onClick={() => this.removeChapter()} style={{fontWeight:'unset', borderRadius:'2px'}}>Supprimer le chapitre</button>;
      if (chapters.length !== 1) {
        if (currentChapter === chapters.length) {
          return <>
            {pagination}
            <div style={{ width:'150px', display:'flex', flexDirection:'column' }}>
              {nextChapterButton}
              {removeChapterButton}
            </div>
          </>;
        }
        return <>{pagination}</>;
      }
      return <div style={{ width:'250px'}}>{nextChapterButton}</div>;

    };
    return <>
      <div style={{display:'flex', justifyContent:'flex-start',  width:'100%', height:'55px', marginTop:'10px'}}>
        <div style={{width:'150px', marginRight:'10px'}}>
          <Button className="button-primary" disabled={saveDisabled} onClick={() => this.saveCourse()} style={{height:'100%'}}>Sauvegarder</Button>
        </div>
        <div style={{width:'150px'}}>
          <button className="btn-black" onClick={() => this.saveAndExit()} style={{ margin:'0', width:'100%', height:'100%', fontWeight:'unset' }}>Terminer</button>
        </div>
      </div>
      <div className="page-chapter-container">
        <h1 style={{ width:'220px', margin:'0', fontSize:'32px'}}>Chapitre {currentChapter}:</h1>
        <Input className="chapter-title" placeholder="Saisissez un titre" bordered={false} value={chapters[currentChapter-1].title} onChange={this.handleChapterTitleInput.bind(this)}/>    
      </div>
      <div className="page-content">
        {blockArray}
        <div style={{display:'flex', width:'100%', height:'106px',justifyContent:'space-between', alignItems:'center', marginTop:'50px'}}>
          {chapterFooter()}
        </div>
      </div>      
    </>;
  }

}
export default Chapter;