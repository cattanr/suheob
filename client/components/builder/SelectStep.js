import React from 'react';
import { Select, Button } from 'antd';
import Router from 'next/router';

const { Option } = Select;

class SelectStep extends React.Component {

  handleChange(value) {
    const { courseid } = this.props;
    Router.push(`/instructor/course/${courseid}/manage/${value}`);
  }

  render() {
    const { option, courseid } = this.props;
    return <>
      <div style={{ display:'flex', flexDirection:'row', justifyContent:'flex-start', alignItems:'flex-start' }}>
        <div className="select-step-container">
          <Select defaultValue={option} style={{ width: 270, marginRight:'10px', marginBottom:'10px' }} onChange={this.handleChange.bind(this)}>
            <Option value="basics">Information de base du cours</Option>
            <Option value="build">Construire son cours</Option>
            <Option value="pricing">Définir le prix du cours</Option>
          </Select>
          <Button className="button-primary" onClick={() => Router.push(`/instructor/course/${courseid}/manage/review`)} style={{ width:'270px', height:'32px' }}>Envoyer pour évaluation</Button>
        </div>
      </div>
    </>;
  }

}
export default SelectStep;