import Block from './Block';
import BlockModal from './BlockModal';
import ImageModal from './ImageModal';
import VideoModal from './VideoModal';
import Chapter from './Chapter';
import SelectStep from './SelectStep';

export default {
  Block,
  BlockModal,
  ImageModal,
  VideoModal,
  Chapter,
  SelectStep,
};