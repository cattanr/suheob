import React from 'react';
import { Input, Modal, Button, Upload, Form, message } from 'antd';
import { getKeycloakInstance } from '@react-keycloak/ssr';

class ImageModal extends React.Component {

  constructor() {
    super();
    this.formRef = React.createRef();
    this.state = {
      activeTab: 'download',
    };
  }

  submitForm(formData) {
    const { data, onUpdate } = this.props;
    data.content = formData.url;
    onUpdate(data);
  }

  activeTabContent(activeTab) {
    const { courseid, data, onUpdate } = this.props;
    const keycloakInstance = getKeycloakInstance();
    if (activeTab === 'download') {
      const uploadProps = {
        action: `${process.env.NEXT_PUBLIC_URL_SERVER}/course/${courseid}/image/upload`,
        headers: { Authorization: `Bearer ${keycloakInstance.token}` },
        maxCount: 1,
        onChange: (info) => {
          if (info.file.status === 'done') {
            message.success(`${info.file.name} file uploaded successfully`);
            if (info.file.response) {
              data.content = info.file.response;
              onUpdate(data); 
            }
          } else if (info.file.status === 'error') {
            message.error(`${info.file.name} file upload failed.`);
          }
        },
      };
      return <div style={{ marginTop:'24px', display:'flex', justifyContent:'center', alignItems:'center' }}>
        <Upload {...uploadProps}>
          <Button className="btn-black">Choisissez une image</Button>
        </Upload>
      </div>;
    }
    if (activeTab === 'embed') {
      return <div style={{ paddingTop:'15px' }} >
        <Form 
          layout="vertical"
          onFinish={this.submitForm.bind(this)}
          ref={this.formRef}
        >
          <Form.Item name="url" 
            rules={[
              { required:true, message:'Un url valide est obligatoire' },
              { type:'url', message:'Ce champs doit être un url valide' }
            ]} 
          >
            <Input id="embed-input" placeholder="Collez le lien de l'image"/>  
          </Form.Item>
        </Form>
        <div style={{ marginTop:'10px', display:'flex', justifyContent:'center', alignItems:'center' }}>
          <button className="btn-black" style={{ width:'80%' }} onClick={() => this.formRef.current.submit()}>Intégrer l'image</button>
        </div>
      </div>;
    }

  }

  render() {
    const { activeTab } = this.state;
    const { visible, onCancel } = this.props;
    return <>
      <Modal 
        visible={visible}
        centered
        keyboard={true}
        closable={false}
        mask={false}
        width="324"
        onCancel={onCancel}
        zIndex={11}
        footer={null}
      >
        <div style={{ display:'flex', flexDirection:'column', width:'clamp(220px, 100%, 350px)', minWidth:'180px', height:'100%', maxHeight:'420px', padding:'0px' }}>
          <div style={{ display:'flex', fontSize:'14px', boxShadow:'rgb(55 53 47 / 9%) 0px 1px 0px', width:'100%' }}>
            <div className={ activeTab === 'download' ? 'modal-item active' : 'modal-item not-active'} onClick={() => this.setState({activeTab: 'download'})}>Télécharger</div>
            <div className={ activeTab === 'embed' ? 'modal-item active' : 'modal-item not-active'} onClick={() => this.setState({activeTab: 'embed'})}>Lien d'une image</div>
          </div>
          { this.activeTabContent(activeTab) }
        </div>
      </Modal>
    </>;
  }

}
export default ImageModal;