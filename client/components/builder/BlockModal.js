import React from 'react';
import { Modal } from 'antd';
import { v4 as uuidv4 } from 'uuid';


class BlockModal extends React.Component {

  render() {
    const { visible, onCancel, addBlock } = this.props;
    const nextBlockId = uuidv4();
    return <>
      {/* <div style={{position: 'absolute', top:'0px', left:'0px', width:'100%', height:'100%', display:'flex', flexDirection:'column', justifyContent:'flex-start', alignItems:'flex-start'}}>
        <div style={{position:'relative', display:'flex', alignItems:'center', flexDirection:'column-reverse', left:'0px', top:'4px'}}>
          <div style={{borderRadius:'3px', background:'white', position:'relative', maxWidth:'calc(100vw - 24px)', boxShadow:'rgb(15 15 15 / 5%) 0px 0px 0px 1px, rgb(15 15 15 / 10%) 0px 3px 6px, rgb(15 15 15 / 20%) 0px 9px 24px'}}>
            <div style={{ display:'flex', flexDirection:'column', width:'324px', minWidth:'180px', maxWidth:'calc(100vw - 24px)', height:'100%', maxHeight:'40vh', overflow: 'hidden auto' }}> */}
      <Modal 
        visible={visible}
        centered
        keyboard={true}
        closable={false}
        mask={false}
        width="324"
        onCancel={onCancel}
        zIndex={10}
        footer={null}
      >
        <div style={{ height:'40vh', overflow: 'hidden auto' }}>
          <div className="basic-blocks" role="button" onClick={() => addBlock(nextBlockId, 'Text')}>
            <div style={{ display:'flex', alignItems:'center', lineHeight:'120%', width:'100%', minHeight:'45px', fontSize:'14px', padding:'4px 0px' }}>
              <div style={{ display:'flex', alignItems:'center', justifyContent:'center', marginLeft:'14px', alignSelf:'flex-start'  }}>
                <div>
                  <img src="/img/block-text.png" style={{display:'block', objectFit:'cover', borderRadius:'3px', background:'white', width:'46px', height:'46px', boxShadow:'rgb(15 15 15 / 10%) 0px 0px 0px 1px'}}/>
                </div>
                <div style={{marginLeft:'8px', marginRight:'14px', }}>
                  <div style={{display:'flex', alignItems:'center'}}><span>Texte</span></div>
                  <div style={{marginTop:'2px', color:'rgba(55, 53, 47, 0.6)'}}>Commencez à écrire</div>
                </div>
              </div>
            </div>
          </div>
          <div className="basic-blocks" onClick={() => addBlock(nextBlockId, 'BigTitle')}>
            <div style={{ display:'flex', alignItems:'center', lineHeight:'120%', width:'100%', minHeight:'45px', fontSize:'14px', padding:'4px 0px' }}>
              <div style={{ display:'flex', alignItems:'center', justifyContent:'center', marginLeft:'14px', alignSelf:'flex-start'  }}>
                <div>
                  <img src="/img/block-h1.png" style={{display:'block', objectFit:'cover', borderRadius:'3px', background:'white', width:'46px', height:'46px', boxShadow:'rgb(15 15 15 / 10%) 0px 0px 0px 1px'}}/>
                </div>
                <div style={{marginLeft:'8px', marginRight:'14px', }}>
                  <div style={{display:'flex', alignItems:'center'}}><span>Titre 1</span></div>
                  <div style={{marginTop:'2px', color:'rgba(55, 53, 47, 0.6)'}}>Titre de grosse taille</div>
                </div>
              </div>
            </div>
          </div>
          <div className="basic-blocks" onClick={() => addBlock(nextBlockId, 'MediumTitle')}>
            <div style={{ display:'flex', alignItems:'center', lineHeight:'120%', width:'100%', minHeight:'45px', fontSize:'14px', padding:'4px 0px' }}>
              <div style={{ display:'flex', alignItems:'center', justifyContent:'center', marginLeft:'14px', alignSelf:'flex-start'  }}>
                <div>
                  <img src="/img/block-h2.png" style={{display:'block', objectFit:'cover', borderRadius:'3px', background:'white', width:'46px', height:'46px', boxShadow:'rgb(15 15 15 / 10%) 0px 0px 0px 1px'}}/>
                </div>
                <div style={{marginLeft:'8px', marginRight:'14px', }}>
                  <div style={{display:'flex', alignItems:'center'}}><span>Titre 2</span></div>
                  <div style={{marginTop:'2px', color:'rgba(55, 53, 47, 0.6)'}}>Titre de taille moyenne</div>
                </div>
              </div>
            </div>
          </div>
          <div className="basic-blocks" onClick={() => addBlock(nextBlockId, 'SmallTitle')}>
            <div style={{ display:'flex', alignItems:'center', lineHeight:'120%', width:'100%', minHeight:'45px', fontSize:'14px', padding:'4px 0px' }}>
              <div style={{ display:'flex', alignItems:'center', justifyContent:'center', marginLeft:'14px', alignSelf:'flex-start'  }}>
                <div>
                  <img src="/img/block-h3.png" style={{display:'block', objectFit:'cover', borderRadius:'3px', background:'white', width:'46px', height:'46px', boxShadow:'rgb(15 15 15 / 10%) 0px 0px 0px 1px'}}/>
                </div>
                <div style={{marginLeft:'8px', marginRight:'14px', }}>
                  <div style={{display:'flex', alignItems:'center'}}><span>Titre 3</span></div>
                  <div style={{marginTop:'2px', color:'rgba(55, 53, 47, 0.6)'}}>Titre de petite taille</div>
                </div>
              </div>
            </div>
          </div>
          <div className="basic-blocks" onClick={() => addBlock(nextBlockId, 'Image')}>
            <div style={{ display:'flex', alignItems:'center', lineHeight:'120%', width:'100%', minHeight:'45px', fontSize:'14px', padding:'4px 0px' }}>
              <div style={{ display:'flex', alignItems:'center', justifyContent:'center', marginLeft:'14px', alignSelf:'flex-start'  }}>
                <div>
                  <img src="/img/block-image.png" style={{display:'block', objectFit:'cover', borderRadius:'3px', background:'white', width:'46px', height:'46px', boxShadow:'rgb(15 15 15 / 10%) 0px 0px 0px 1px'}}/>
                </div>
                <div style={{marginLeft:'8px', marginRight:'14px', }}>
                  <div style={{display:'flex', alignItems:'center'}}><span>Image</span></div>
                  <div style={{marginTop:'2px', color:'rgba(55, 53, 47, 0.6)'}}>Téléchargez ou intégrez via un lien</div>
                </div>
              </div>
            </div>
          </div>
          <div className="basic-blocks" onClick={() => addBlock(nextBlockId, 'Video')}>
            <div style={{ display:'flex', alignItems:'center', lineHeight:'120%', width:'100%', minHeight:'45px', fontSize:'14px', padding:'4px 0px' }}>
              <div style={{ display:'flex', alignItems:'center', justifyContent:'center', marginLeft:'14px', alignSelf:'flex-start'  }}>
                <div>
                  <img src="/img/video.png" style={{display:'block', objectFit:'cover', borderRadius:'3px', background:'white', width:'46px', height:'46px', boxShadow:'rgb(15 15 15 / 10%) 0px 0px 0px 1px'}}/>
                </div>
                <div style={{marginLeft:'8px', marginRight:'14px', }}>
                  <div style={{display:'flex', alignItems:'center'}}><span>Video</span></div>
                  <div style={{marginTop:'2px', color:'rgba(55, 53, 47, 0.6)'}}>Intégrez une video Youtube, Vimeo...</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    </>;
  }

}
export default BlockModal;