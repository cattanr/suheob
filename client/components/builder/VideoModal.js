import React from 'react';
import axios from 'axios';
import { Input, Modal, Button, Upload, Form, message } from 'antd';



class VideoModal extends React.Component {

  constructor() {
    super();
    this.formRef = React.createRef();
  }

  async getVimeoEmbed(videoUrl) {
    try {
      const url = `https://vimeo.com/api/oembed.json?url=${encodeURI(videoUrl)}`;
      const response = await axios({
        method: 'get',
        url
      });
      const { video_id } = response.data;
      return `https://player.vimeo.com/video/${video_id}`;
    } catch (error) {
      message.error(error.message);
    }
  }

  async submitForm(formData) {
    const { data, onUpdate } = this.props;
    const { url } = formData;
    let content;
    let id;
    switch (true) {
    case url.includes('https://www.youtube.com'):
      id = url.match(/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/)[7];
      content = `https://www.youtube.com/embed/${id}`;
      break;
    case url.includes('https://vimeo.com'):
      content = await this.getVimeoEmbed(url);
      break;
    default:
      break;
    }
    if (content) {
      data.content = content;
      onUpdate(data);
    }
  }

  render() {
    const { visible, onCancel } = this.props;
    return <>
      <Modal 
        visible={visible}
        centered
        keyboard={true}
        closable={false}
        mask={false}
        width="324"
        onCancel={onCancel}
        zIndex={11}
        footer={null}
      >
        <div style={{ display:'flex', flexDirection:'column', width:'250px', minWidth:'180px', height:'100%', maxHeight:'420px', padding:'0px' }}>
          <div style={{ display:'flex', fontSize:'14px', boxShadow:'rgb(55 53 47 / 9%) 0px 1px 0px', width:'100%' }}>
            <div className='modal-item active'>Lien d'une vidéo</div>
          </div>
          <div style={{ paddingTop:'15px' }} >
            <Form 
              layout="vertical"
              onFinish={this.submitForm.bind(this)}
              ref={this.formRef}
            >
              <Form.Item name="url" 
                rules={[
                  { required:true, message:'Un url valide est obligatoire' },
                  { type:'url', message:'Ce champs doit être un url valide' }
                ]} 
              >
                <Input id="embed-input" placeholder="Collez le lien de l'image"/>  
              </Form.Item>
            </Form>
            <div style={{ marginTop:'10px', display:'flex', justifyContent:'center', alignItems:'center' }}>
              <button className="btn-black" style={{ width:'80%' }} onClick={() => this.formRef.current.submit()}>Intégrer une vidéo</button>
            </div>
          </div>
        </div>
      </Modal>
    </>;
  }

}
export default VideoModal;