import React from 'react';
import { LoadingOutlined } from '@ant-design/icons';

class LoadingPage extends React.Component {
  
  render() {
    return <div className="loading-container">
      <div className="loading-icon">
        <LoadingOutlined style={{fontSize:'2000%'}}/>
      </div>
      <div className="loading-text">
        <h2>Chargement en cours ...</h2>
      </div>
    </div>;
  }

}

export default LoadingPage;
