import BigTitle from './BigTitle';
import MediumTitle from './MediumTitle';
import SmallTitle from './SmallTitle';
import Image from './Image';
import Text from './Text';
import Video from './Video';

export default {
  BigTitle,
  MediumTitle,
  SmallTitle,
  Image,
  Text,
  Video
};