import React from 'react';
import ImageModal from '../builder/ImageModal';

class Image extends React.Component {

  constructor() {
    super();
    this.state = {
      visibleModal: false,
    };
  }

  render() {
    const { visibleModal } = this.state;
    const { onChangeModalVisible, onRemove, data, courseid, onUpdate } = this.props;
    const { id:blockId, content } = data;
    return <>
      <div className="tools-container" style={{ paddingTop:'15px' }}>
        <div id={blockId} style={{ display: 'none' }}>
          <div className="icon-container" role="button" onClick={() => onChangeModalVisible(true)}>
            <svg viewbox="0 0 16 16" className="plus">
              <path 
                d="M7.977 14.963c.407 0 .747-.324.747-.723V8.72h5.362c.399 0 .74-.34.74-.747a.746.746 0 00-.74-.738H8.724V1.706c0-.398-.34-.722-.747-.722a.732.732 0 00-.739.722v5.529h-5.37a.746.746 0 00-.74.738c0 .407.341.747.74.747h5.37v5.52c0 .399.332.723.739.723z">
              </path>
            </svg>
          </div>
          <div className="icon-container" role="button" onClick={() => onRemove(blockId)}>
            <svg viewBox="0 0 30 30" className="trash"><path d="M21,5c0-2.2-1.8-4-4-4h-4c-2.2,0-4,1.8-4,4H2v2h2v22h22V7h2V5H21z M13,3h4c1.104,0,2,0.897,2,2h-8C11,3.897,11.897,3,13,3zM24,27H6V7h18V27z M16,11h-2v12h2V11z M20,11h-2v12h2V11z M12,11h-2v12h2V11z"></path></svg>
          </div>
        </div>
      </div>
      { content === undefined || content === '' ?
        <div onClick={() => this.setState({visibleModal: true})} style={{ width:'100%', maxWidth:'564px', alignSelf:'center', margin:'4px 0px' }}>
          <div style={{display:'flex'}}>
            <div style={{background:'rgb(242, 241, 238)', width:'100%', borderRadius:'3px'}}>
              <div style={{fontSize:'14px', color:'rgba(55, 53, 47, 0.6)', cursor:'pointer'}}>
                <div style={{ display:'flex', alignItems:'center', textAlign:'left', width:'100%', overflow:'hidden', padding:'12px' }}>
                  <svg viewBox="0 0 30 30" style={{width: '25px', height: '25px', display: 'block', fill: 'rgba(55, 53, 47, 0.4)', flexShrink: '0', backfaceVisibility: 'hidden', marginRight: '12px'}}><path d="M1,4v22h28V4H1z M27,24H3V6h24V24z M18,10l-5,6l-2-2l-6,8h20L18,10z M11.216,17.045l1.918,1.918l4.576-5.491L21.518,20H9 L11.216,17.045z M7,12c1.104,0,2-0.896,2-2S8.104,8,7,8s-2,0.896-2,2S5.896,12,7,12z"></path></svg>
                  <div style={{whiteSpace:'nowrap', overflow:'hidden', textOverflow:'ellipsis'}}>Ajouter une image</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        : <div>
          <img alt="course-image" src={content}/>
        </div>
      }
      <ImageModal 
        courseid={courseid}
        data={data}
        visible={visibleModal}
        onCancel={() => this.setState({visibleModal: false})}
        onUpdate={(block) => {
          this.setState({visibleModal: false});
          onUpdate(block);
        }}
      />
    </>;
  }

}
export default Image;