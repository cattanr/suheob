import React from 'react';
import VideoModal from '../builder/VideoModal';

class Video extends React.Component {

  constructor() {
    super();
    this.state = {
      visibleModal: false,
    };
  }

  render() {
    const { visibleModal } = this.state;
    const { onChangeModalVisible, onRemove, data, courseid, onUpdate } = this.props;
    const { id:blockId, content } = data;
    return <>
      <div className="tools-container" style={{ paddingTop:'15px' }}>
        <div id={blockId} style={{ display: 'none' }}>
          <div className="icon-container" role="button" onClick={() => onChangeModalVisible(true)}>
            <svg viewbox="0 0 16 16" className="plus">
              <path 
                d="M7.977 14.963c.407 0 .747-.324.747-.723V8.72h5.362c.399 0 .74-.34.74-.747a.746.746 0 00-.74-.738H8.724V1.706c0-.398-.34-.722-.747-.722a.732.732 0 00-.739.722v5.529h-5.37a.746.746 0 00-.74.738c0 .407.341.747.74.747h5.37v5.52c0 .399.332.723.739.723z">
              </path>
            </svg>
          </div>
          <div className="icon-container" role="button" onClick={() => onRemove(blockId)}>
            <svg viewBox="0 0 30 30" className="trash"><path d="M21,5c0-2.2-1.8-4-4-4h-4c-2.2,0-4,1.8-4,4H2v2h2v22h22V7h2V5H21z M13,3h4c1.104,0,2,0.897,2,2h-8C11,3.897,11.897,3,13,3zM24,27H6V7h18V27z M16,11h-2v12h2V11z M20,11h-2v12h2V11z M12,11h-2v12h2V11z"></path></svg>
          </div>
        </div>
      </div>
      { content === undefined || content === '' ? 
        <div onClick={() => this.setState({visibleModal: true})} style={{ width:'100%', maxWidth:'564px', alignSelf:'center', margin:'4px 0px' }}>
          <div style={{display:'flex'}}>
            <div style={{background:'rgb(242, 241, 238)', width:'100%', borderRadius:'3px'}}>
              <div style={{fontSize:'14px', color:'rgba(55, 53, 47, 0.6)', cursor:'pointer'}}>
                <div style={{ display:'flex', alignItems:'center', textAlign:'left', width:'100%', overflow:'hidden', padding:'12px 36px 12px 12px' }}>
                  <svg viewBox="0 0 30 30" class="video" style={{width: '25px', height: '25px', display: 'block', fill: 'rgba(55, 53, 47, 0.4)', flexShrink: '0', backfaceVisibility: 'hidden', marginRight: '12px'}}><path d="M2,2v26h26V2H2z M26,6h-2V4h2V6z M22,14H8V4h14V14z M6,10H4V8h2V10z M6,12v2H4v-2H6z M6,16v2H4v-2H6z M6,20v2H4v-2H6z M8,16 h14v10H8V16z M24,20h2v2h-2V20z M24,18v-2h2v2H24z M24,14v-2h2v2H24z M24,10V8h2v2H24z M6,4v2H4V4H6z M4,24h2v2H4V24z M24,26v-2h2v2 H24z"></path></svg>
                  <div style={{whiteSpace:'nowrap', overflow:'hidden', textOverflow:'ellipsis'}}>Ajouter un lien vidéo</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        : 
        <div className="iframe-container">
          <iframe 
            src={content} 
            frameBorder="0" 
            sandbox="allow-scripts allow-popups allow-top-navigation-by-user-activation allow-forms allow-same-origin" 
            allowFullScreen
          />        
        </div>
      }
      <VideoModal
        courseid={courseid}
        data={data}
        visible={visibleModal}
        onCancel={() => this.setState({visibleModal: false})}
        onUpdate={(block) => {
          this.setState({visibleModal: false});
          onUpdate(block);
        }}
      />
    </>;
  }

}
export default Video;