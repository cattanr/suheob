import React from 'react';
import { Card, Rate } from 'antd';
import Image from 'next/image';

const { Meta } = Card;

class CartItem extends React.Component {

  render() {
    const { item, onRemove, hover } = this.props;
    let cartItem;
    if (hover === true) {
      cartItem = <>
        <div className="hover-cart-container">
          <img src={item.main_image} alt="product-image" width="100" height="100"/>
          <div className="hover-cart-info">
            <span className="hover-cart-title">
              { item.title }
            </span>
            <span className="hover-cart-price">
            { item.price !== 0 ? `${item.price} €` : 'Gratuit' }
            </span>
            <a className="hover-cart-remove" onClick={() => onRemove(item)}>Supprimer</a>
          </div>
        </div>
      </>;
    }
    else {
      const descriptionContent = () => {
        return <>
          <div>
            <span>{ item.creator }</span>
          </div>
          <div>
            <span style={{color:'#B4690E'}}>5
              <Rate disabled allowHalf defaultValue="5" style={{ color:'#E59719', fontSize:'90%', paddingLeft:'8px'}}/>
            </span>
          </div>
          <div>
            <strong>{ item.price !== 0 ? `${item.price} €` : 'Gratuit' }</strong>
          </div>
          <div>
            <a href="#" style={{color:'#5624d0'}} onClick={() => onRemove(item)}>Supprimer</a>
          </div>
        </>;
      };
      cartItem =
        <Card
          style={{width:'100%', height:'290px', maxWidth:'300px', marginTop:'10px'}}
          cover={
            <img alt="test" src={item.main_image} style={{ width:'100%', height:'130px', padding:'2px' }}/>
          }
        >
          <Meta title={item.title} description={descriptionContent()}/>
        </Card>;
     
    }
    return <>
      {cartItem}
    </>;
  }

}
export default CartItem;