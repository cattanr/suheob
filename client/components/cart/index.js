import CartContent from "./CartContent";
import CartItem from "./CartItem";

export default {
  CartContent,
  CartItem,
};