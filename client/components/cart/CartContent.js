import React from 'react';
import CartItem from './CartItem';
import Router from 'next/router';


class CartContent extends React.Component {

  removeFromCart(itemToRemove) {
    const { content, onUpdate } = this.props;
    const cart = content.filter(item => item.id && item.id !== itemToRemove.id);
    localStorage.setItem('cart', JSON.stringify(cart));
    onUpdate(cart);
  }

  computeTotalPrice(cart) {
    return cart.reduce((acc, current) => acc + current.price, 0);
  }

  render() {
    const { content, hover } = this.props;
    let cart;
    const total = this.computeTotalPrice(content);
    if (content.length !== 0) {
      cart = content.map((product, index) =>
        <CartItem item={product} key={index} hover={hover} onRemove={(item) => this.removeFromCart(item)}></CartItem>
      );
    } else {
      if (hover) {
        cart = <>
          <div className="hover-cart-empty">
            <p>Votre panier est vide</p>
            <a className="hover-cart-remove" href="/">Continuer vos achats</a>
          </div>
        </>;
      }
      else {
        cart = <>
          <div className="empty-cart-container">
            <div className="empty-cart-content">
              <img alt="empty-cart" src="/img/empty-shopping-cart.jpg" width="480" height="360"/>
              <p>Votre panier est vide</p>
              <button className="button-primary" onClick={() => Router.push('/') } style={{width:'180px'}}>Continuer vos achats</button>
            </div>
          </div>
        </>;
      }
    }
    return <>
      {cart}
      {content.length !== 0 && hover ? 
        <>
          <div style={{ padding:'12px' }}>
            <span style={{fontWeight:'700'}}>Total: {total} €</span>
            <button onClick={() => Router.push('/cart')} style={{width: '100%', height:'40px', marginTop:'10px', color:'white', backgroundColor:'#1c1c1f'}}>Accéder au panier</button>
          </div>
        </>
        : <></>}
    </>;
  }

}
export default CartContent;