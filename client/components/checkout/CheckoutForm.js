import React from 'react';
import getStripe from '../../utils/get-stripe';
import axios from 'axios';
import { getKeycloakInstance } from '@react-keycloak/ssr';
import { message } from 'antd';
import Router from 'next/router';

class CheckoutForm extends React.Component {

  constructor() {
    super();
    this.state = {
      loading: false,
    };
  }

  async handleSubmit(event) {
    try {
      this.setState({loading:true});
      const keycloakInstance = getKeycloakInstance();
      const { cart }  = this.props;
      const { email } = keycloakInstance.idTokenParsed;
      const data = { cart, email };
      const url = `${process.env.NEXT_PUBLIC_URL_SERVER}/create_checkout_sessions`;
      event.preventDefault();
      const response = await axios({
        method: 'post',
        url,
        data,
        headers: { Authorization: `Bearer ${keycloakInstance.token}` }
      });
      if (response.statusCode === 500) {
        message.error('Service momentanément indisponible, veuillez réessayer ultérieurement');
        return;
      }
      if (response.data.type === 'payment' && response.data.sessionId) {
        const stripe = await getStripe();
        localStorage.removeItem('cart');
        const { error } = await stripe.redirectToCheckout({ sessionId: response.data.sessionId });
        if (error) {
          message.error(error.message);
        }
        this.setState({loading:false});
        
      }
      else if (response.data.type === 'free') {
        localStorage.removeItem('cart');
        Router.push('/payment?status=success');
      }
    } catch (error) {
      message.error(error.message);
    }
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit.bind(this)}>
        <button className="button-primary" type="submit" disabled={this.state.loading}>Procéder au paiement</button>
      </form>
    );
  }

}
export default CheckoutForm;