import React from 'react';
import Link from 'next/link';
import { Result } from 'antd';

class PaymentResult extends React.Component {

  render() {
    const { status } = this.props;
    const title = status => {
      if (status === 'success') {
        return "Paiement réussi";
      }
      else if (status === 'error') return "Paiement échoué";
    };
    const subTitle = status => {
      if (status === 'success') {
        return "Merci pour votre achat";
      }
      else if (status === 'error') return "Une erreur est survenu pendant le paiement, merci de réessayer";
    };
    const button = status => {
      if (status === 'success') {
        return (
          <Link href="/home/my-courses/learning">
            <a>
              <button className="button-primary">
                Aller à mon espace
              </button>
            </a>
          </Link>
        );
      }
      else if (status === 'error') {
        return (
          <Link href="/">
            <a>
              <button className="button-primary">
                Retour à l'acceuil
              </button>
            </a>
          </Link>
        );
      }
    };
    return (
      <div style={{display:'flex', alignItems:'center', justifyContent:'center', height:'100vh'}}>
        { status === 'success' || status === 'error' ? 
          <Result status={status} title={title(status)} subTitle={subTitle(status)} style={{ width:'100%', maxWidth:'500px'}}
            extra={button(status)}
          /> : <>
          <Link href="/">
            <a style={{ width:'100%', maxWidth:'500px' }}>
              <button className="button-primary">
                Retour à l'acceuil
              </button>
            </a>
          </Link>
          </>
        }
      </div>
    );
  }

}
export default PaymentResult;