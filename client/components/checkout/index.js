import CheckoutForm from './CheckoutForm';
import PaymentResult from './PaymentResult';

export default {
  CheckoutForm,
  PaymentResult,
};