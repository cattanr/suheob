import React from 'react';
import { Modal, Button } from 'antd';
import { InfoCircleOutlined } from '@ant-design/icons';

class InformationModal extends React.Component {

  render() {
    const { visible, onOk, onClick } = this.props;
    return <>
      <Modal
        centered
        visible={visible}
        keyboard={true}
        closable={false}
        onOk={onOk}
        onCancel={onOk}
        okButtonProps={{style:{backgroundColor: '#1c1c1f', borderColor:'#1c1c1f'}}}
        cancelButtonProps={{style: { display: 'none' }}}
        zIndex={999}
      >
        <div>
          <h2>Bienvenue sur le portfolio de Romain Cattaneo.</h2>
          <span>Ce projet est un site de cours en ligne avec comme main feature un builder de cours via des blocks prédéfinis. Vous pouvez la tester sur le backoffice formateur.</span>
        </div>
      </Modal>
      <div id="infoBtn">
        <Button 
          danger 
          shape="round" 
          disabled={visible} 
          className="informationBtn"  
          icon={<InfoCircleOutlined/>} 
          size="middle"
          onClick={onClick}>
          <span id="infoLabel">
            Information
          </span>
        </Button>
      </div>
    </>;

  }

}

export default InformationModal;