import React from 'react';
import Link from 'next/link'

class TeacherHeader extends React.Component {

  render() {
    const { activeTab } = this.props;
    return <div style={{ backgroundColor:'#1c1c1f', margin:'-24px -24px 0px -24px', padding:'0px 15px', height:'auto' }}>
      <div style={{ paddingTop:'34px'}}>
        <h1 style={{ color: 'white'}}><span>Mon apprentissage</span></h1>
      </div>
      <div className="nav-container">
        <ul className="nav" style={{ height:'30px' }}>
          <li className={ activeTab === 'all-courses' ? 'active' : 'not-active' }>
            <Link href="/home/my-courses/learning">
              <a >
                Tous les cours
              </a>
            </Link>
          </li>
          <li className={ activeTab === 'wishlist' ? 'active' : 'not-active' }>
            <Link href="/home/my-courses/wishlist">
              <a>
                Liste de souhaits
              </a>
            </Link>
          </li>
        </ul>
      </div>
    </div>;
  }

}
export default TeacherHeader;