import React from "react";
import { Layout, Menu, Drawer, Divider, Popover } from "antd";
import {
  PlaySquareOutlined,
  MailOutlined,
  BarChartOutlined,
  ToolOutlined,
  MenuOutlined,
  RightOutlined,
  LeftOutlined,
} from '@ant-design/icons';
import Link from 'next/link';
import { getKeycloakInstance } from '@react-keycloak/ssr';

const { Sider } = Layout;

class BackOfficeNav extends React.Component {

  constructor() {
    super();
    this.state = {
      collapsed: true,
      visible: false,
      childrenDrawer: false,
      keycloakInstance: undefined,
    };

  }

  componentDidMount() {
    this.setState({ keycloakInstance: getKeycloakInstance() });
  }

  render() {
    const { keycloakInstance } = this.state;
    const drawerTitle = () => {
      if (keycloakInstance?.authenticated) {
        return <button onClick={() => this.setState({childrenDrawer: true})} style={{display:'flex', alignItems:'center', width:'100%', height:'auto'}}>
          <div className="drawer-badge">
            <span>
              {keycloakInstance.idTokenParsed.family_name[0].toUpperCase()}{keycloakInstance.idTokenParsed.given_name[0].toUpperCase()}
            </span>
          </div>
          <span style={{ width:'144px', height:'auto' }}>
            <span style={{ display:'block', marginLeft:'16px', textAlign:'start', fontWeight:'600' }}>
              Bonjour, {keycloakInstance.idTokenParsed.name}
            </span>
            <span style={{display:'block', marginLeft:'16px',  color:'#6a6f73', textAlign:'start', fontWeight:'300', fontSize:'14px' }}>Bienvenue</span>
          </span>
          <RightOutlined />
        </button>;
      }
      else {
        return <button className="login-btn" onClick={() => keycloakInstance.login()}>Se connecter / S'incrire</button>;
      }
    };
    const childrenDrawerTitle = (
      <button onClick={() => this.setState({childrenDrawer: false})} style={{display:'flex',  textAlign:'left', alignItems:'center', width:'100%', height:'auto'}}>
        <LeftOutlined/>
        <div style={{marginLeft:'16px'}}>
          <span>Profil</span>
        </div>
      </button>);
    const userBadgeContent = (<>
      <div style={{ width:'300px', padding:'24px' }}>
        <Link href="/instructor/courses">
          <a>
            <div style={{ borderBottom: '1px solid #d1d7dc' }}>
                  <div className="drawer-badge" style={{ marginBottom:'12px' }}>
                    <span>
                      {keycloakInstance?.idTokenParsed?.family_name[0].toUpperCase()}{keycloakInstance?.idTokenParsed?.given_name[0].toUpperCase()}
                    </span>
                  </div>
                  <div style={{ display:'inline-block', marginLeft:'10px'}}>
                    <span style={{ fontWeight:'700' }}>{keycloakInstance?.idTokenParsed?.family_name} {keycloakInstance?.idTokenParsed?.given_name}</span>
                    <span style={{ fontSize:'12px', display:'block' }}>{keycloakInstance?.idTokenParsed?.email}</span>
                  </div>
            </div>
          </a>
        </Link>
        <div style={{ marginTop:'12px' }}>
          <div style={{ marginBottom:'12px' }}>Aide</div>
          <div style={{ marginBottom:'12px' }}><button onClick={() => keycloakInstance.logout()}>Se déconnecter</button></div>
        </div>
      </div>
      </>
    );
    return (<>
      <div className="backoffice-navbar">
        <button className="backoffice-burger-btn" onClick={() => this.setState({visible: !this.state.visible})}>
          <MenuOutlined style={{ verticalAlign:'middle', color:'white' }}/>
        </button>
        {keycloakInstance?.authenticated ?
          <div className="backoffice-visible">
            <Link href={'/home/my-courses/learning'}>
              <a>
                <div className="participant-button">
                  <span>Mon Apprentissage</span>
                </div>
              </a>
            </Link>
            <Popover placement="bottomRight" content={userBadgeContent}>
              <button>
                <div className="drawer-badge" style={{width:'46px', height:'46px', margin:'8px 12px', verticalAlign:'center'}}>
                  <span style={{fontSize:'15px'}}>
                    {keycloakInstance.idTokenParsed.family_name[0].toUpperCase()}{keycloakInstance.idTokenParsed.given_name[0].toUpperCase()}
                  </span>
                </div> 
              </button>
            </Popover>
          </div>
          : <></>
        }
      </div>
      <Drawer
        title={drawerTitle()}
        className="backoffice-drawer"
        placement="left"
        onClose={() => this.setState({visible:false})}
        visible={this.state.visible}
        closable={false}
        headerStyle={{backgroundColor:'#F6F9FA', padding:'16px'}}
      >
        <Drawer
          title={childrenDrawerTitle}
          placement="left"
          visible={this.state.childrenDrawer}
          closable={false}
          onClose={() => this.setState({ childrenDrawer:false })}
          headerStyle={{backgroundColor:'#F6F9FA', padding:'16px'}}
        >
          <Menu mode="inline" style={{ height: '100%', borderRight: 0 }}
            defaultOpenKeys={this.props.openOrSelected} selectedKeys={this.props.openOrSelected}>
            <Menu.Item key="public-profile">
              <span>Profil public</span>
            </Menu.Item>
            <Menu.Item key="modify-profil">
              <span>Modifier le profil</span>
            </Menu.Item>
          </Menu>
        </Drawer>
        <Menu mode="inline" style={{ height: '100%', borderRight: 0 }}
          defaultOpenKeys={this.props.openOrSelected} selectedKeys={this.props.openOrSelected}>
          <Menu.Item key="student">
            <Link href={'/home/my-courses/learning'}><a><span style={{color:'#5624d0'}}>Passer en vue Participant</span></a></Link>
          </Menu.Item>
          <Divider/> 
          <Menu.Item key="course">
            <span>Cours</span>
          </Menu.Item>
          <Menu.Item key="communication">
            <span>Communication</span>
          </Menu.Item>
          <Menu.Item key="performance">
            <span>Performance</span>
          </Menu.Item>
          <Menu.Item key="tools">
            <span>Outils</span>
          </Menu.Item>
        </Menu>
      </Drawer>
      <div className="backoffice-sider">
        <Sider
          collapsed={this.state.collapsed}
          onMouseEnter={ () => {
            if (this.state.collapsed) {
              this.setState({
                collapsed: false
              });
            }
          }}
          onMouseLeave={ () => {
            if (!this.state.collapsed) {
              this.setState({
                collapsed: true
              });
            }
          }}
        >
          <div className="backoffice-logo">
            <Link href="/instructor/courses">
              <a><img className="logo" src="/img/dark-logo.png" width="40" height="30"/></a>
            </Link>
          </div>
          <Menu theme="dark" defaultSelectedKeys={["courses"]} mode="inline">
            <Menu.Item key="courses" icon={<PlaySquareOutlined />}>
              Cours
            </Menu.Item>
            <Menu.Item key="communication" icon={<MailOutlined />}>
              Communication
            </Menu.Item>
            <Menu.Item key="performance" icon={<BarChartOutlined />}>
              Performances
            </Menu.Item>
            <Menu.Item key="tools" icon={<ToolOutlined />}>
              Outils
            </Menu.Item>
          </Menu>
        </Sider>

      </div>
    </>
    );
  }

}
export default BackOfficeNav;
