import React from "react";
import Link from 'next/link';
import { Drawer, Menu, Button, Popover, Layout, Badge, Divider } from "antd";
import { MenuOutlined, SearchOutlined, ShoppingCartOutlined, GlobalOutlined, RightOutlined, LeftOutlined } from '@ant-design/icons';
import { getKeycloakInstance } from '@react-keycloak/ssr';
import CartContent from "../cart/CartContent";

class NavBar extends React.Component {

  constructor() {
    super();
    this.state = {
      visible: false,
      childrenDrawer: false,
      keycloakInstance: undefined,
    };
  }

  componentDidMount() {
    this.setState({ keycloakInstance: getKeycloakInstance() });
  }

  render() {
    const { cart, onUpdate } = this.props;
    const { keycloakInstance } = this.state;
    const drawerTitle = () => {
      if (keycloakInstance?.authenticated) {
        return <button onClick={() => this.setState({childrenDrawer: true})} style={{display:'flex', alignItems:'center', width:'100%', height:'auto'}}>
          <div className="drawer-badge">
            <span>
              {keycloakInstance.idTokenParsed.family_name[0].toUpperCase()}{keycloakInstance.idTokenParsed.given_name[0].toUpperCase()}
            </span>
          </div>
          <span style={{ width:'144px', height:'auto' }}>
            <span style={{ display:'block', marginLeft:'16px', textAlign:'start', fontWeight:'600' }}>
              Bonjour, {keycloakInstance.idTokenParsed.name}
            </span>
            <span style={{display:'block', marginLeft:'16px',  color:'#6a6f73', textAlign:'start', fontWeight:'300', fontSize:'14px' }}>Bienvenue</span>
          </span>
          <RightOutlined />
        </button>;
      }
      else {
        return <button className="login-btn" onClick={() => keycloakInstance.login()}>Se connecter / S'incrire</button>;
      }
    };
    const childrenDrawerTitle = (
      <button onClick={() => this.setState({childrenDrawer: false})} style={{display:'flex',  textAlign:'left', alignItems:'center', width:'100%', height:'auto'}}>
        <LeftOutlined/>
        <div style={{marginLeft:'16px'}}>
          <span>Menu</span>
        </div>
      </button>);
    const teacherContent = (
      <div style={{padding: '16px', maxWidth:'330px'}}>
        <p style={{fontSize:'16px', fontWeight:'700'}}>Devenez enseignant et partagez vos connaissances avec le monde entier.</p>
        <Button type="primary" href={`/teaching`} block={true} style={{backgroundColor:'#1c1c1f', borderColor: '#1c1c1f'}}>En savoir plus</Button>
      </div>
    );
    const userBadgeContent = (<>
      <div style={{ width:'300px', padding:'24px' }}>
        <Link href="/">
          <a>
            <div style={{ borderBottom: '1px solid #d1d7dc' }}>
                  <div className="drawer-badge" style={{ marginBottom:'12px' }}>
                    <span>
                      {keycloakInstance?.idTokenParsed?.family_name[0].toUpperCase()}{keycloakInstance?.idTokenParsed?.given_name[0].toUpperCase()}
                    </span>
                  </div>
                  <div style={{ display:'inline-block', marginLeft:'10px'}}>
                    <span style={{ fontWeight:'700' }}>{keycloakInstance?.idTokenParsed?.family_name} {keycloakInstance?.idTokenParsed?.given_name}</span>
                    <span style={{ fontSize:'12px', display:'block' }}>{keycloakInstance?.idTokenParsed?.email}</span>
                  </div>
            </div>
          </a>
        </Link>
        <div style={{ borderBottom: '1px solid #d1d7dc', marginTop:'12px' }}>
          <div style={{ marginBottom:'12px' }}><Link href="/home/my-courses/learning"><a>Mon apprentissage</a></Link></div>
          <div style={{ marginBottom:'12px' }}><Link href="/cart"><a>Mon panier</a></Link></div>
          <div style={{ marginBottom:'12px' }}><Link href="/instructor/courses"><a>Tableau de bord du formateur</a></Link></div>
        </div>
        <div style={{ marginTop:'12px' }}>
          <div style={{ marginBottom:'12px' }}>Aide</div>
          <div style={{ marginBottom:'12px' }}><button onClick={() => keycloakInstance.logout()}>Se déconnecter</button></div>
        </div>
      </div>
      </>
    );
    return <>
      <Layout>
        <div className="navbar">
          <button className="burger-btn" onClick={() => this.setState({visible: !this.state.visible})}>
            <MenuOutlined style={{ verticalAlign:'middle' }}/>
          </button>
          <div className="logo-container">
            <Link href="/">
              <a><img className="logo" src="/img/logo.png" width="255" height="255"/></a>  
            </Link>
          </div>
          <div className="nav-item">
            <button className="button-text-primary">
              <SearchOutlined style={{verticalAlign:'middle', fontSize:'120%'}}/>
            </button>
            { keycloakInstance?.authenticated ?
              <Link href="/instructor/courses">
                <a>
                  <div className="desktop-visible button-text-primary" style={{alignItems:'center'}}>
                    <span>Vue Formateur</span>
                  </div>
                </a>
              </Link>
              :
                <Link href="/teaching">
                  <a>
                    <Popover placement="bottom" content={teacherContent} arrowPointAtCenter>
                      <span className="desktop-visible button-text-primary" style={{padding:'0px 12px'}}>
                        Enseigner
                      </span>
                    </Popover>  
                  </a>
                </Link>
            }
            <Link href="/cart">
              <a className="mobile-visible" style={{padding:'0px 12px'}}>
                <Badge color="#a435ef" count={cart.length}>
                  <ShoppingCartOutlined className="button-text-primary" style={{verticalAlign:'middle', fontSize:'120%', padding:'0'}}/>
                </Badge>
              </a>
            </Link>
            <div className="desktop-visible">
              <Popover placement="bottomRight" content={<CartContent content={cart} hover={true} onUpdate={onUpdate}/>}>
                <Badge color="#a435ef" count={cart.length} offset={[-10, 0]}>
                  <Link href="/cart">
                    <a style={{padding:'0px 12px'}}>
                      <ShoppingCartOutlined className="button-text-primary" style={{verticalAlign:'middle', fontSize:'120%', padding:'0'}}/>
                    </a>
                  </Link>
                </Badge>
              </Popover>
            </div>
            <button className="desktop-visible button-text-primary">
              <GlobalOutlined style={{ verticalAlign:'middle', fontSize:'120%' }}/>
            </button>
            {keycloakInstance?.authenticated ?
              <Popover className="desktop-visible" placement="bottomRight" content={userBadgeContent}>
                <button>
                  <div className="drawer-badge" style={{width:'32px', height:'32px', margin:'8px 12px', verticalAlign:'center'}}>
                    <span style={{fontSize:'12px'}}>
                      {keycloakInstance.idTokenParsed.family_name[0].toUpperCase()}{keycloakInstance.idTokenParsed.given_name[0].toUpperCase()}
                    </span>
                  </div>
                </button>
              </Popover>
              : <button className="desktop-visible btn-outline" onClick={() => keycloakInstance.login()}  style={{ margin: '0 12px'}}>
                Login/ Register
              </button>
            }
          </div>
          <Drawer
            title={drawerTitle()}
            placement="left"
            onClose={() => this.setState({visible:false})}
            visible={this.state.visible}
            closable={false}
            headerStyle={{backgroundColor:'#F6F9FA', padding:'16px'}}
          >
            <Drawer
              title={childrenDrawerTitle}
              placement="left"
              visible={this.state.childrenDrawer}
              closable={false}
              onClose={() => this.setState({ childrenDrawer:false })}
              headerStyle={{backgroundColor:'#F6F9FA', padding:'16px'}}
            >
              <Menu mode="inline" style={{ height: '100%', borderRight: 0 }}
                defaultOpenKeys={this.props.openOrSelected} selectedKeys={this.props.openOrSelected}>
                <Menu.ItemGroup key="account" title="Compte">
                  <Menu.Item key="account-security">
                    <span>Paramètres du compte</span>
                  </Menu.Item>
                  <Menu.Item key="history">
                    <span>Historique des achats</span>
                  </Menu.Item>
                </Menu.ItemGroup>
                <Divider/>
                <Menu.ItemGroup key="profile" title="Profil">
                  <Menu.Item key="public-profile">
                    <span>Profil public</span>
                  </Menu.Item>
                  <Menu.Item key="modify-profil">
                    <span>Modifier le profil</span>
                  </Menu.Item>
                  <Menu.Item key="logout">
                    <button onClick={() => keycloakInstance.logout()}>Se déconnecter</button>
                  </Menu.Item>
                </Menu.ItemGroup>              
              </Menu>
            </Drawer>
            <Menu mode="inline" style={{ height: '100%', borderRight: 0 }}
              defaultOpenKeys={this.props.openOrSelected} selectedKeys={this.props.openOrSelected}>
              { keycloakInstance?.authenticated ?
                <>
                  <Menu.Item key="instructor">
                      <Link href={`/instructor/courses`}><a><span style={{color:'#5624d0'}}>Passer en vue Formateur</span></a></Link>
                    </Menu.Item>
                  <Divider/>  
                  <Menu.ItemGroup key="profile" title="Profil">
                    <Menu.Item key="my-learning">
                      <Link href={`/home/my-courses/learning`}><a><span>Mon apprentissage</span></a></Link>
                    </Menu.Item>
                  </Menu.ItemGroup>
                </>            
                :
                <Menu.ItemGroup key="buy" title="Achats">
                  <Menu.Item key="buy-courses">
                    <Link href="/"><a><span>Acheter des cours</span></a></Link>
                  </Menu.Item>
                </Menu.ItemGroup>
              }
              <Divider/>  
              <Menu.ItemGroup key="other-services" title="Autres services">
                <Menu.Item key="teacher">
                  <Link href={`/teaching`}><a><span>Devenir enseignant</span></a></Link>
                </Menu.Item>
                <Menu.Item key="referal">
                  <span>Invitez des amis</span>
                </Menu.Item>
                <Menu.Item key="helper">
                  <span>Aide</span>
                </Menu.Item>
                <Menu.Item key="traduction">
                  <GlobalOutlined/>
                </Menu.Item>
              </Menu.ItemGroup>
            </Menu>
          </Drawer>
        </div>
      </Layout>
    </>;
  }

}

export default NavBar;