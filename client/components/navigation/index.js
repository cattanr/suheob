import NavBar from './NavBar';
import InformationModal from './InformationModal';
import BackOfficeNav from './BackOfficeNav';
import TeacherHeader from './TeacherHeader';


export default {
  NavBar,
  InformationModal,
  BackOfficeNav,
  TeacherHeader,
};