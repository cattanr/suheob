import React from 'react';
import { Rate, Card } from 'antd';

const { Meta } = Card;

class CourseCard extends React.Component {

  render() {
    const { content } = this.props;
    const descriptionContent = (
      <>
        <div>
          <span>{ content.creator }</span>
        </div>
        <div>
          <span style={{color:'#B4690E'}}>5
            <Rate disabled allowHalf defaultValue="5" style={{ color:'#E59719', fontSize:'90%', paddingLeft:'8px'}}/>
          </span>
        </div>
        <div>
          <strong>{ content.price !== 0 ? `${content.price} €` : 'Gratuit' }</strong>
        </div>
      </>
    );
    return <>
      <Card
        style={{width:'100%', height:'100%', maxWidth:'300px'}}
        cover={
          <img alt="test" src={content.main_image} style={{ width:'100%', height:'130px', padding:'2px' }}/>
        }
      >
        <Meta title={content.title} description={descriptionContent}/>
      </Card>
    </>;
  }

}

export default CourseCard;