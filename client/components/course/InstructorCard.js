import React from 'react';
import { Alert } from 'antd';
import Link from 'next/link';
import { FormOutlined, LoadingOutlined } from '@ant-design/icons';

class InstructorCard extends React.Component {

  formatAlert(status) {
    let content;
    switch (status) {
      case 'DRAFT':
        content = <Alert icon={<FormOutlined/>} message="Brouillon" type="warning" banner style={{ maxWidth:'110px' }}/>;
        break;
      case 'LIVE':
        content = <Alert message="En ligne" type="success" banner style={{ maxWidth:'110px' }}/>;
        break;
      case 'IN_REVIEW':
        content = <Alert icon={<LoadingOutlined/>} message="En attente de revue" type="info" banner style={{ maxWidth:'200px' }}/>;
        break;
      default:
        content = <Alert icon={<FormOutlined/>} message="Brouillon" type="warning" banner style={{ maxWidth:'110px' }}/>;
        break;
    }
    return content;
  }

  render() {
    const { course } = this.props;
    return <>
      <Link href={`/instructor/course/${course.id}/manage/basics`}>
        <a>
          <div className="instructor-card-container">
                <div className="instructor-image-container">
                  <img alt="course-thumbnail" src={ course?.mainImage === null ? "/img/default-image.jpg" : course.mainImage } width="750" height="422" style={{ width:'100%', height:'100%', padding:'12px' }}/>
                </div>
                <div style={{ display:'flex', flex:'1'}}>
                  <div style={{padding:'12px 16px', height:'100%', display: 'flex', flexDirection:'column', justifyContent:'space-between'}}>
                    <div style={{ minHeight:'16px', marginBottom:'16px' }}>
                      { course.title === null ?
                        <span style={{ color:'#d1d7dc' }}>Titre manquant</span>
                        : <span>{course.title}</span>
                      }
                    </div>
                    <div>
                      { this.formatAlert(course.status) }
                    </div>
                  </div>
                </div>
          </div>
        </a>
      </Link>
    </>;
  }

}
export default InstructorCard;