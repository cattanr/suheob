import React from 'react';
import Link from 'next/link';

class ChapterCard extends React.Component {
  render() {
    const { courseid, chapternumber, content } = this.props;
    return <>
      <Link href={`/course/${courseid}/learn/chapter/${chapternumber}`}>
        <a>
          <div className="" style={{ backgroundColor:'#f7f9fa', border:'1px solid #d1d7dc', width:'100%', padding:'16px', marginTop:'8px' }}>
            <span>{chapternumber} - {content.title}</span>
          </div>
        </a>
      </Link>
    </>;
  }
}
export default ChapterCard;