import React from 'react';
import { Card, Progress, Empty } from 'antd';
import Link from 'next/link';

const { Meta } = Card;

class MyCourseCard extends React.Component {

  render() {
    const { content } = this.props;
    const descriptionContent = (
      <>
        <div>
          <span>
            { content?.creator }
          </span>
        </div>
        <div>
          <Progress percent={50} showInfo={false}/>
          <div style={{ marginTop:'5px'}}>
            <span>DÉBUTER LE COURS</span>
          </div>
        </div>
      </>
    );
    return <>
      {content ?
        <Link href={`/course/${content?.id}/learn`}>
          <a>
            <Card
              style={{ width: '100%', height: '243px', boxShadow:'0 4px 2px -2px rgb(28 29 31 / 20%)' }}
              cover={
                <img alt="test" src={content?.main_image} style={{ width:'100%', height:'130px', padding:'2px' }}/>
              }
            >
              <Meta title={content?.title} description={descriptionContent}/>
            </Card>
          </a>
        </Link>
        : <div><Empty image={Empty.PRESENTED_IMAGE_SIMPLE} /></div>
      }
    </>;
  }

}
export default MyCourseCard;