import CourseCarousel from "./CourseCarousel";
import CourseCard from "./CourseCard";
import MyCourseCard from "./MyCourseCard";
import InstructorCard from "./InstructorCard";
import ChapterCard from "./ChapterCard";

export default {
  CourseCard,
  CourseCarousel,
  MyCourseCard,
  InstructorCard,
  ChapterCard
};