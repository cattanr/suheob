import React from 'react';
import CourseCard from './CourseCard';
import Link from 'next/link';
import { Empty } from 'antd';


class CourseCarousel extends React.Component {

  render() {
    const { courses } = this.props;
    return <>
      <div className="carousel-grid">
        { courses ?
          courses.map((course) => {
            return <Link href={`/course/${course.id}`} key={course.id}>
            <a>
              <div className="carousel-items">
                <CourseCard content={course}/>
              </div>
            </a>
            </Link>
          })
          : <div><Empty image={Empty.PRESENTED_IMAGE_SIMPLE} /></div>
        }
      </div>
    </>;
  }

}
export default CourseCarousel;

