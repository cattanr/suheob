import React from 'react';
import { Form, message } from 'antd';
import FormItems from './FormItems';
import { getKeycloakInstance } from '@react-keycloak/ssr';
import Router from 'next/router';
import CourseService from '../../services/Course.service';
import { Pause } from '../../utils';

class CourseForm extends React.Component {

  constructor() {
    super();
    this.formRef = React.createRef();
    this.state = {
      keycloakInstance: getKeycloakInstance() || undefined,
    };
  }

  async componentDidMount() {
    this.setState({ keycloakInstance: getKeycloakInstance() });
    await this.actualizeCourse();
  }

  async submitForm(formData) {
    try {
      const { keycloakInstance, course } = this.state;
      await CourseService.updateCourseByBearer(keycloakInstance.token, course.id, formData);
      await Pause(1000);
      message.success('Votre cours a bien été actualisées');
      Router.push(`/instructor/course/${course.id}/manage/build`);
    } catch (error) {
      message.error('Votre cours n\'a pas pu être mis à jour');
    }    
  }

  async actualizeCourse() {
    try {
      const { courseid } = this.props;
      const { keycloakInstance } = this.state;
      const course = await CourseService.getInstructorCourseById(keycloakInstance.token, courseid);
      if (course === null) {
        Router.push('/instructor/courses');
      }
      this.setState({ course });
      if (this.formRef.current !== null) this.formRef.current.setFieldsValue(course);
    } catch (error) {
      message.error('Nous ne pouvons pas récuperer vos informations, veuillez vérifier votre connexion');
    }
  }

  render() {
    const { keycloakInstance } = this.state;
    const { course } = this.state;
    return <>
      {course ? 
        <Form 
          layout="vertical"
          initialValues={course}
          ref={this.formRef}
          onFinish={this.submitForm.bind(this)}
        >
          <FormItems keycloaktoken={keycloakInstance?.token} course={course}/>
        </Form> : <></>
      }
      <div style={{ display:'flex', justifyContent:'flex-start', alignItems:'center' }}>
        <button className="btn-black" onClick={() => this.formRef.current.submit()} style={{ width:'clamp(100px, 100%, 300px)' }}>Sauvegarder</button>
      </div>
    </>;
  }

}
export default CourseForm;