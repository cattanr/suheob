import React from 'react';
import { Form, Input, Upload, Button, message } from 'antd';
import { UploadOutlined } from '@ant-design/icons';

const { TextArea } = Input;

class FormItems extends React.Component {

  previewFile(file) {
    const preview = document.getElementById('output');
    const reader  = new FileReader();
    reader.addEventListener("load", function () {
      preview.src = reader.result;
    }, false);
    if (file) {
      reader.readAsDataURL(file);
    }
  }

  render() {
    const { keycloaktoken, course } = this.props;
    const uploadProps = {
      action: `${process.env.NEXT_PUBLIC_URL_SERVER}/course/${course.id}/upload`,
      headers: { Authorization: `Bearer ${keycloaktoken}` },
      maxCount: 1,
      onChange: (info) => {
        if (info.file.status === 'done') {
          message.success(`${info.file.name} file uploaded successfully`);
          this.previewFile(info.file.originFileObj);
        } else if (info.file.status === 'error') {
          message.error(`${info.file.name} file upload failed.`);
        }
      },
    };
    return <>
      <Form.Item name="title" label="Titre du cours"
        rules={[{ message: 'Veuillez renseigner le titre du cours'}]}>
        <Input placeholder="Saisissez le titre de votre cours."/>
      </Form.Item>
      <Form.Item name="description" label="Description du cours"
        rules={[{ message: 'Veuillez renseigner la description du cours !'}]}>
        <TextArea placeholder="Saisissez la description de votre cours." autoSize={{ minRows: 3, maxRows: 5 }}/>
      </Form.Item>
      <img id="output" src={course && course.mainImage ? course.mainImage : "/img/default-image.jpg"} alt="default" width="750" height="422" style={{ maxWidth:'370px', width:'clamp(250px, 100%, 370px)', height:'auto', marginBottom:'10px' }}/>
      <Form.Item name="upload">
        <Upload {...uploadProps}>
          <Button icon={<UploadOutlined/>}>Télécharger le fichier</Button>
        </Upload>
      </Form.Item>
    </>;
  }

}
export default FormItems;