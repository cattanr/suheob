export async function Pause(time) {
  await new Promise(res => setTimeout(res, time));
}